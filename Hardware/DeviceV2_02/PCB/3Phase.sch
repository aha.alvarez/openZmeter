EESchema Schematic File Version 4
LIBS:3Phase-cache
EELAYER 26 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Components:MCP6004 U1
U 3 1 5BD043B1
P 5975 3275
F 0 "U1" H 6025 3075 50  0000 L CNN
F 1 "MCP6004" H 5775 3000 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5925 3375 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 6025 3475 50  0001 C CNN
	3    5975 3275
	-1   0    0    -1  
$EndComp
$Comp
L Components:MCP6004 U1
U 2 1 5BD0441A
P 5200 2825
F 0 "U1" H 5200 3025 50  0000 L CNN
F 1 "MCP6004" H 5025 2575 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5150 2925 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 5250 3025 50  0001 C CNN
	2    5200 2825
	-1   0    0    -1  
$EndComp
$Comp
L Components:MCP6004 U1
U 4 1 5BD04495
P 5975 4875
F 0 "U1" H 5975 5075 50  0000 L CNN
F 1 "MCP6004" H 5825 4650 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5925 4975 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 6025 5075 50  0001 C CNN
	4    5975 4875
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R8
U 1 1 5BD04895
P 4650 2825
F 0 "R8" V 4730 2825 50  0000 C CNN
F 1 "100h" V 4650 2825 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4580 2825 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 4650 2825 50  0001 C CNN
	1    4650 2825
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R11
U 1 1 5BD0490E
P 5850 6975
F 0 "R11" V 5930 6975 50  0000 C CNN
F 1 "10k" V 5850 6975 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 6975 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5850 6975 50  0001 C CNN
	1    5850 6975
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R14
U 1 1 5BD04949
P 4375 7600
F 0 "R14" V 4455 7600 50  0000 C CNN
F 1 "100h" V 4375 7600 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4305 7600 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 4375 7600 50  0001 C CNN
	1    4375 7600
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R15
U 1 1 5BD04974
P 4975 8150
F 0 "R15" V 5055 8150 50  0000 C CNN
F 1 "10K" V 4975 8150 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4905 8150 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 4975 8150 50  0001 C CNN
	1    4975 8150
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R10
U 1 1 5BD049A1
P 4950 6975
F 0 "R10" V 5030 6975 50  0000 C CNN
F 1 "10K" V 4950 6975 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4880 6975 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 4950 6975 50  0001 C CNN
	1    4950 6975
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R7
U 1 1 5BD049D0
P 4375 6475
F 0 "R7" V 4455 6475 50  0000 C CNN
F 1 "100h" V 4375 6475 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4305 6475 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 4375 6475 50  0001 C CNN
	1    4375 6475
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R13
U 1 1 5BD04A47
P 5475 6575
F 0 "R13" V 5555 6575 50  0000 C CNN
F 1 "10K" V 5475 6575 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5405 6575 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5475 6575 50  0001 C CNN
	1    5475 6575
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R17
U 1 1 5BD04A80
P 6450 6975
F 0 "R17" V 6530 6975 50  0000 C CNN
F 1 "10k" V 6450 6975 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6380 6975 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 6450 6975 50  0001 C CNN
	1    6450 6975
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R16
U 1 1 5BD04AB5
P 6450 5275
F 0 "R16" V 6530 5275 50  0000 C CNN
F 1 "10k" V 6450 5275 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6380 5275 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 6450 5275 50  0001 C CNN
	1    6450 5275
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R12
U 1 1 5BD04AEC
P 4575 5050
F 0 "R12" V 4655 5050 50  0000 C CNN
F 1 "10k" V 4575 5050 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4505 5050 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 4575 5050 50  0001 C CNN
	1    4575 5050
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R9
U 1 1 5BD04B25
P 5475 4875
F 0 "R9" V 5555 4875 50  0000 C CNN
F 1 "10k" V 5475 4875 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5405 4875 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5475 4875 50  0001 C CNN
	1    5475 4875
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R6
U 1 1 5BD04B60
P 5850 5275
F 0 "R6" V 5930 5275 50  0000 C CNN
F 1 "10k" V 5850 5275 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 5275 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5850 5275 50  0001 C CNN
	1    5850 5275
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R3
U 1 1 5BD04C05
P 4350 4775
F 0 "R3" V 4430 4775 50  0000 C CNN
F 1 "100h" V 4350 4775 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4280 4775 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 4350 4775 50  0001 C CNN
	1    4350 4775
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R5
U 1 1 5BD04C4A
P 6450 3675
F 0 "R5" V 6530 3675 50  0000 C CNN
F 1 "10k" V 6450 3675 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6380 3675 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 6450 3675 50  0001 C CNN
	1    6450 3675
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R4
U 1 1 5BD04C8B
P 4875 3050
F 0 "R4" V 4955 3050 50  0000 C CNN
F 1 "10k" H 4875 3050 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4805 3050 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 4875 3050 50  0001 C CNN
	1    4875 3050
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R1
U 1 1 5BD04CCE
P 5650 3100
F 0 "R1" V 5730 3100 50  0000 C CNN
F 1 "10k" V 5650 3100 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5580 3100 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5650 3100 50  0001 C CNN
	1    5650 3100
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R2
U 1 1 5BD04D27
P 5850 3675
F 0 "R2" V 5930 3675 50  0000 C CNN
F 1 "10k" V 5850 3675 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 3675 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5850 3675 50  0001 C CNN
	1    5850 3675
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R19
U 1 1 5BD04D6E
P 5850 8150
F 0 "R19" V 5930 8150 50  0000 C CNN
F 1 "10K" V 5850 8150 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 8150 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5850 8150 50  0001 C CNN
	1    5850 8150
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R22
U 1 1 5BD04DB7
P 6450 8150
F 0 "R22" V 6530 8150 50  0000 C CNN
F 1 "10K" V 6450 8150 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6380 8150 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 6450 8150 50  0001 C CNN
	1    6450 8150
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R24
U 1 1 5BD04E02
P 1025 9350
F 0 "R24" V 1105 9350 50  0000 C CNN
F 1 "200k" V 1025 9350 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 955 9350 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1025 9350 50  0001 C CNN
	1    1025 9350
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R29
U 1 1 5BD04E4F
P 1725 9350
F 0 "R29" V 1805 9350 50  0000 C CNN
F 1 "200k" V 1725 9350 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1655 9350 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1725 9350 50  0001 C CNN
	1    1725 9350
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R18
U 1 1 5BD04E9E
P 1025 10000
F 0 "R18" V 1105 10000 50  0000 C CNN
F 1 "200k" V 1025 10000 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 955 10000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1025 10000 50  0001 C CNN
	1    1025 10000
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R21
U 1 1 5BD04EEF
P 1025 10650
F 0 "R21" V 1105 10650 50  0000 C CNN
F 1 "200k" V 1025 10650 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 955 10650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1025 10650 50  0001 C CNN
	1    1025 10650
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R25
U 1 1 5BD04F42
P 1375 9350
F 0 "R25" V 1455 9350 50  0000 C CNN
F 1 "200k" V 1375 9350 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1305 9350 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1375 9350 50  0001 C CNN
	1    1375 9350
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R27
U 1 1 5BD04F97
P 1375 10000
F 0 "R27" V 1455 10000 50  0000 C CNN
F 1 "200k" V 1375 10000 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1305 10000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1375 10000 50  0001 C CNN
	1    1375 10000
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R20
U 1 1 5BD04FEE
P 5475 7700
F 0 "R20" V 5555 7700 50  0000 C CNN
F 1 "10K" V 5475 7700 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5405 7700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 5475 7700 50  0001 C CNN
	1    5475 7700
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R23
U 1 1 5BD05047
P 6500 1650
F 0 "R23" V 6580 1650 50  0000 C CNN
F 1 "10k" H 6500 1650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6430 1650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 6500 1650 50  0001 C CNN
	1    6500 1650
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R26
U 1 1 5BD050A2
P 1725 10650
F 0 "R26" V 1805 10650 50  0000 C CNN
F 1 "200k" V 1725 10650 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1655 10650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1725 10650 50  0001 C CNN
	1    1725 10650
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R28
U 1 1 5BD050FF
P 1375 10650
F 0 "R28" V 1455 10650 50  0000 C CNN
F 1 "200k" V 1375 10650 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1305 10650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1375 10650 50  0001 C CNN
	1    1375 10650
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R30
U 1 1 5BD0515E
P 1725 10000
F 0 "R30" V 1805 10000 50  0000 C CNN
F 1 "200k" V 1725 10000 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1655 10000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/200pcs-1206-1-SMD-Resistor-200K-ohm-Chip-Resistor-0-25W-1-4W-200K-ohms/1361740_32863752563.html" H 1725 10000 50  0001 C CNN
	1    1725 10000
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R31
U 1 1 5BD051BF
P 1725 9175
F 0 "R31" V 1805 9175 50  0000 C CNN
F 1 "2.2k" V 1725 9175 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1655 9175 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-1-3K-1K3-1-5K-1K5/624710_32953475610.html" H 1725 9175 50  0001 C CNN
	1    1725 9175
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R32
U 1 1 5BD05222
P 1725 9825
F 0 "R32" V 1805 9825 50  0000 C CNN
F 1 "2.2k" V 1725 9825 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1655 9825 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-1-3K-1K3-1-5K-1K5/624710_32953475610.html" H 1725 9825 50  0001 C CNN
	1    1725 9825
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R34
U 1 1 5BD05287
P 1725 10475
F 0 "R34" V 1805 10475 50  0000 C CNN
F 1 "2.2k" V 1725 10475 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1655 10475 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-1-3K-1K3-1-5K-1K5/624710_32953475610.html" H 1725 10475 50  0001 C CNN
	1    1725 10475
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R35
U 1 1 5BD052EE
P 1950 8225
F 0 "R35" V 2030 8225 50  0000 C CNN
F 1 "10k" V 1950 8225 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1880 8225 50  0001 C CNN
F 3 "" H 1950 8225 50  0001 C CNN
	1    1950 8225
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R33
U 1 1 5BD0542F
P 5000 1650
F 0 "R33" V 5080 1650 50  0000 C CNN
F 1 "33k" V 5000 1650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4930 1650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-5-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32952050119.html" H 5000 1650 50  0001 C CNN
	1    5000 1650
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R36
U 1 1 5BD0550F
P 5350 1450
F 0 "R36" V 5430 1450 50  0000 C CNN
F 1 "240k" V 5350 1450 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5280 1450 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 5350 1450 50  0001 C CNN
	1    5350 1450
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C6
U 1 1 5BD060D2
P 4475 3025
F 0 "C6" H 4485 3095 50  0000 L CNN
F 1 "100nF" H 4485 2945 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4475 3025 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 4475 3025 50  0001 C CNN
	1    4475 3025
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C4
U 1 1 5BD0621D
P 4150 5000
F 0 "C4" H 4160 5070 50  0000 L CNN
F 1 "100nF" H 4160 4920 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4150 5000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 4150 5000 50  0001 C CNN
	1    4150 5000
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C2
U 1 1 5BD06296
P 4175 6650
F 0 "C2" H 4185 6720 50  0000 L CNN
F 1 "100nF" H 4185 6570 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4175 6650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 4175 6650 50  0001 C CNN
	1    4175 6650
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C3
U 1 1 5BD06311
P 5700 1700
F 0 "C3" H 5710 1770 50  0000 L CNN
F 1 "100nF 50v" H 5710 1620 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5700 1700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 5700 1700 50  0001 C CNN
	1    5700 1700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C5
U 1 1 5BD0638E
P 4700 1150
F 0 "C5" H 4710 1220 50  0000 L CNN
F 1 "C_Small" H 4710 1070 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4700 1150 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 4700 1150 50  0001 C CNN
	1    4700 1150
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C7
U 1 1 5BD06419
P 4200 7775
F 0 "C7" H 4210 7845 50  0000 L CNN
F 1 "100nF" H 4210 7695 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4200 7775 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 4200 7775 50  0001 C CNN
	1    4200 7775
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J1
U 1 1 5BD094EB
P 7200 3125
F 0 "J1" H 7350 3475 50  0000 C CNN
F 1 "95001-6P6C" H 7000 3475 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7200 3125 50  0001 C CNN
F 3 "" H 7200 3125 50  0000 C CNN
F 4 "L1" H 7200 2850 50  0000 C CNN "Name"
	1    7200 3125
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J3
U 1 1 5BD09649
P 7200 6425
F 0 "J3" H 7350 6775 50  0000 C CNN
F 1 "95001-6P6C" H 7000 6775 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7200 6425 50  0001 C CNN
F 3 "" H 7200 6425 50  0000 C CNN
F 4 "L3" H 7200 6125 50  0000 C CNN "Name"
	1    7200 6425
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J4
U 1 1 5BD096FE
P 7200 7550
F 0 "J4" H 7300 7725 50  0000 C CNN
F 1 "95001-6P6C" H 7000 7900 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7200 7550 50  0001 C CNN
F 3 "" H 7200 7550 50  0000 C CNN
F 4 "N" H 7200 7250 50  0000 C CNN "Name"
	1    7200 7550
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J2
U 1 1 5BD095C2
P 7200 4725
F 0 "J2" H 7350 5075 50  0000 C CNN
F 1 "95001-6P6C" H 7000 5075 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7200 4725 50  0001 C CNN
F 3 "" H 7200 4725 50  0000 C CNN
F 4 "L2" H 7200 4450 50  0000 C CNN "Name"
	1    7200 4725
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5BD125E9
P 4475 3150
F 0 "#PWR013" H 4475 2900 50  0001 C CNN
F 1 "GND" H 4475 3000 50  0000 C CNN
F 2 "" H 4475 3150 50  0000 C CNN
F 3 "" H 4475 3150 50  0000 C CNN
	1    4475 3150
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR029
U 1 1 5BD1A139
P 6975 2975
F 0 "#PWR029" H 6975 2825 50  0001 C CNN
F 1 "+5V" V 6975 3150 50  0000 C CNN
F 2 "" H 6975 2975 50  0000 C CNN
F 3 "" H 6975 2975 50  0000 C CNN
	1    6975 2975
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5BD1A83E
P 6950 3275
F 0 "#PWR032" H 6950 3025 50  0001 C CNN
F 1 "GND" V 6875 3375 50  0000 C CNN
F 2 "" H 6950 3275 50  0000 C CNN
F 3 "" H 6950 3275 50  0000 C CNN
	1    6950 3275
	0    1    -1   0   
$EndComp
$Comp
L Components:+1V65 #PWR021
U 1 1 5BD1DAD6
P 7125 3675
F 0 "#PWR021" H 7125 3525 50  0001 C CNN
F 1 "+1V65" H 7100 3825 50  0000 C CNN
F 2 "" H 7125 3675 50  0000 C CNN
F 3 "" H 7125 3675 50  0000 C CNN
	1    7125 3675
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5BD1DD5A
P 5000 5275
F 0 "#PWR018" H 5000 5025 50  0001 C CNN
F 1 "GND" H 5000 5125 50  0000 C CNN
F 2 "" H 5000 5275 50  0000 C CNN
F 3 "" H 5000 5275 50  0000 C CNN
	1    5000 5275
	-1   0    0    -1  
$EndComp
$Comp
L Components:+1V65 #PWR024
U 1 1 5BD26ECB
P 6875 5275
F 0 "#PWR024" H 6875 5125 50  0001 C CNN
F 1 "+1V65" V 6775 5300 50  0000 C CNN
F 2 "" H 6875 5275 50  0000 C CNN
F 3 "" H 6875 5275 50  0000 C CNN
	1    6875 5275
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5BD279CC
P 4150 5175
F 0 "#PWR014" H 4150 4925 50  0001 C CNN
F 1 "GND" H 4150 5025 50  0000 C CNN
F 2 "" H 4150 5175 50  0000 C CNN
F 3 "" H 4150 5175 50  0000 C CNN
	1    4150 5175
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR036
U 1 1 5BD27A56
P 6975 4875
F 0 "#PWR036" H 6975 4625 50  0001 C CNN
F 1 "GND" V 6875 4875 50  0000 C CNN
F 2 "" H 6975 4875 50  0000 C CNN
F 3 "" H 6975 4875 50  0000 C CNN
	1    6975 4875
	0    1    -1   0   
$EndComp
$Comp
L power:+5V #PWR033
U 1 1 5BD27DE4
P 6975 4575
F 0 "#PWR033" H 6975 4425 50  0001 C CNN
F 1 "+5V" V 6900 4675 50  0000 C CNN
F 2 "" H 6975 4575 50  0000 C CNN
F 3 "" H 6975 4575 50  0000 C CNN
	1    6975 4575
	0    -1   1    0   
$EndComp
$Comp
L Components:MCP6004 U3
U 2 1 5BD2D4E2
P 4900 6475
F 0 "U3" H 4900 6675 50  0000 L CNN
F 1 "MCP6004" H 4850 6275 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4850 6575 50  0001 C CNN
F 3 "" H 4950 6675 50  0000 C CNN
	2    4900 6475
	-1   0    0    -1  
$EndComp
$Comp
L Components:MCP6004 U3
U 4 1 5BD2D609
P 5975 7700
F 0 "U3" H 5600 7925 50  0000 L CNN
F 1 "MCP6004" H 5825 7475 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5925 7800 50  0001 C CNN
F 3 "" H 6025 7900 50  0000 C CNN
	4    5975 7700
	-1   0    0    -1  
$EndComp
$Comp
L Components:MCP6004 U3
U 1 1 5BD2D6F4
P 4900 7600
F 0 "U3" H 4900 7800 50  0000 L CNN
F 1 "MCP6004" H 4900 7400 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4850 7700 50  0001 C CNN
F 3 "" H 4950 7800 50  0001 C CNN
	1    4900 7600
	-1   0    0    -1  
$EndComp
$Comp
L Components:MCP6004 U3
U 3 1 5BD2D7EF
P 5975 6575
F 0 "U3" H 5975 6425 50  0000 L CNN
F 1 "MCP6004" H 5800 6300 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5925 6675 50  0001 C CNN
F 3 "" H 6025 6775 50  0000 C CNN
	3    5975 6575
	-1   0    0    -1  
$EndComp
$Comp
L Components:+1V65 #PWR022
U 1 1 5BD3C362
P 6975 6975
F 0 "#PWR022" H 6975 6825 50  0001 C CNN
F 1 "+1V65" V 7075 6950 50  0000 C CNN
F 2 "" H 6975 6975 50  0000 C CNN
F 3 "" H 6975 6975 50  0000 C CNN
	1    6975 6975
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR040
U 1 1 5BD3C576
P 7025 6575
F 0 "#PWR040" H 7025 6325 50  0001 C CNN
F 1 "GND" V 6925 6500 50  0000 C CNN
F 2 "" H 7025 6575 50  0000 C CNN
F 3 "" H 7025 6575 50  0000 C CNN
	1    7025 6575
	0    1    -1   0   
$EndComp
$Comp
L power:+5V #PWR037
U 1 1 5BD3D5D5
P 6975 6275
F 0 "#PWR037" H 6975 6125 50  0001 C CNN
F 1 "+5V" V 6900 6350 50  0000 C CNN
F 2 "" H 6975 6275 50  0000 C CNN
F 3 "" H 6975 6275 50  0000 C CNN
	1    6975 6275
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5BD3E154
P 4175 6800
F 0 "#PWR015" H 4175 6550 50  0001 C CNN
F 1 "GND" H 4175 6650 50  0000 C CNN
F 2 "" H 4175 6800 50  0000 C CNN
F 3 "" H 4175 6800 50  0000 C CNN
	1    4175 6800
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5BD4142B
P 4200 7925
F 0 "#PWR016" H 4200 7675 50  0001 C CNN
F 1 "GND" H 4200 7775 50  0000 C CNN
F 2 "" H 4200 7925 50  0000 C CNN
F 3 "" H 4200 7925 50  0000 C CNN
	1    4200 7925
	-1   0    0    -1  
$EndComp
$Comp
L Components:+1V65 #PWR023
U 1 1 5BD41EEF
P 7175 8150
F 0 "#PWR023" H 7175 8000 50  0001 C CNN
F 1 "+1V65" V 7250 8225 50  0000 C CNN
F 2 "" H 7175 8150 50  0000 C CNN
F 3 "" H 7175 8150 50  0000 C CNN
	1    7175 8150
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5BD4205A
P 5000 7925
F 0 "#PWR020" H 5000 7675 50  0001 C CNN
F 1 "GND" H 5000 7775 50  0000 C CNN
F 2 "" H 5000 7925 50  0000 C CNN
F 3 "" H 5000 7925 50  0000 C CNN
	1    5000 7925
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR044
U 1 1 5BD42FF9
P 6950 7700
F 0 "#PWR044" H 6950 7450 50  0001 C CNN
F 1 "GND" V 6825 7650 50  0000 C CNN
F 2 "" H 6950 7700 50  0000 C CNN
F 3 "" H 6950 7700 50  0000 C CNN
	1    6950 7700
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R46
U 1 1 5BD9FCD3
P 6625 6700
F 0 "R46" V 6705 6700 50  0000 C CNN
F 1 "1M" V 6625 6700 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6555 6700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6625 6700 50  0001 C CNN
	1    6625 6700
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R45
U 1 1 5BD9FD98
P 6775 6700
F 0 "R45" V 6855 6700 50  0000 C CNN
F 1 "1M" V 6775 6700 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6705 6700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6775 6700 50  0001 C CNN
	1    6775 6700
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R48
U 1 1 5BDAF4B1
P 6625 7775
F 0 "R48" V 6705 7775 50  0000 C CNN
F 1 "1M" V 6625 7775 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6555 7775 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6625 7775 50  0001 C CNN
	1    6625 7775
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R47
U 1 1 5BDAF58A
P 6775 7775
F 0 "R47" V 6855 7775 50  0000 C CNN
F 1 "1M" V 6775 7775 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6705 7775 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6775 7775 50  0001 C CNN
	1    6775 7775
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CP-3Phase-rescue C9
U 1 1 5BDB6F21
P 2450 1550
F 0 "C9" H 2475 1650 50  0000 L CNN
F 1 "3.3uF 450v" H 2475 1450 50  0001 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 2488 1400 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/ELECYINGFO-100pcs-3-3uF-450V-RUBYCON-BXC-10x13mm-High-Ripple-Current-Long-Life-450V3-3uF-Aluminum/1596219_32889253375.html" H 2450 1550 50  0001 C CNN
	1    2450 1550
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CP-3Phase-rescue C8
U 1 1 5BDB7050
P 2050 1550
F 0 "C8" H 2075 1650 50  0000 L CNN
F 1 "3.3uF 450v" H 2075 1450 50  0001 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 2088 1400 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/ELECYINGFO-100pcs-3-3uF-450V-RUBYCON-BXC-10x13mm-High-Ripple-Current-Long-Life-450V3-3uF-Aluminum/1596219_32889253375.html" H 2050 1550 50  0001 C CNN
	1    2050 1550
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:VIPer22-3Phase-rescue U6
U 1 1 5BDB6955
P 3000 1450
F 0 "U6" H 2850 1675 50  0000 R CNN
F 1 "VIPer22" H 3100 1675 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3000 875 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/VIPer22A-viper22-VIPER22A-DIP8-100pcs/4420053_32895985764.html" H 3000 1450 50  0001 C CNN
	1    3000 1450
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:L-3Phase-rescue L1
U 1 1 5BDB7C0C
P 2250 1300
F 0 "L1" V 2200 1300 50  0000 C CNN
F 1 "470uH 350mA" H 2325 1300 50  0001 C CNN
F 2 "Inductor_THT:L_Axial_L12.0mm_D5.0mm_P5.08mm_Vertical_Fastron_MISC" H 2250 1300 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-LOT-1W-color-ring-inductance-DIP-0510-470UH-Inductor/323553_32732378325.html" H 2250 1300 50  0001 C CNN
	1    2250 1300
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase-rescue:Thermistor_NTC-3Phase-rescue TH1
U 1 1 5BDB864F
P 1850 1150
F 0 "TH1" V 1675 1150 50  0000 C CNN
F 1 "NTC 10D7" H 1975 1150 50  0001 C CNN
F 2 "Varistor:RV_Disc_D7mm_W3.4mm_P5mm" H 1850 1200 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-Thermistor-NTC10D-5-10D-5-10D5-5MM-Diameter-Negative-Temperature-coefficient/1596219_32866984932.html" H 1850 1200 50  0001 C CNN
	1    1850 1150
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:Varistor-3Phase-rescue RV1
U 1 1 5BDB8784
P 1800 1550
F 0 "RV1" V 1925 1550 50  0000 C CNN
F 1 "07D391K" H 1675 1550 50  0001 C CNN
F 2 "Varistor:RV_Disc_D7mm_W3.4mm_P5mm" V 1730 1550 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-lot-Varistor-Resistor-7D431K-7D-431K-Voltage-Dependent-Resistor/1762106_32460712919.html " H 1800 1550 50  0001 C CNN
	1    1800 1550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5BDB9E15
P 3350 1900
F 0 "#PWR011" H 3350 1650 50  0001 C CNN
F 1 "GND" H 3350 1750 50  0000 C CNN
F 2 "" H 3350 1900 50  0000 C CNN
F 3 "" H 3350 1900 50  0000 C CNN
	1    3350 1900
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:L-3Phase-rescue L2
U 1 1 5BDBAA07
P 3800 1550
F 0 "L2" V 3750 1550 50  0000 C CNN
F 1 "1mH 300mA" V 3875 1550 50  0001 C CNN
F 2 "Footprints:L_Radial_D10mm_P5.00mm" H 3800 1550 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/50pcs-150uH-220uH-330uH-470uH-1mH-2-2mH-3-3mH-4-7mH-10mH-9-12-dip/1126207_32821675313.html" H 3800 1550 50  0001 C CNN
	1    3800 1550
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C10
U 1 1 5BDBB3A5
P 3600 1400
F 0 "C10" H 3610 1470 50  0000 L CNN
F 1 "100nF 50v" H 3610 1320 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3600 1400 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-shipping-100PCS-capacitance-0402-0-047uF-473-47nF-X7R-25V-10-1-0x0-5mm-1005/511081_1142739709.html" H 3600 1400 50  0001 C CNN
	1    3600 1400
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C11
U 1 1 5BDBB5F6
P 3450 1450
F 0 "C11" H 3460 1520 50  0000 L CNN
F 1 "4.7uF 25v" H 3460 1370 50  0001 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3450 1450 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-LOT-SMD-ceramic-capacitor-0805-2-2UF-225K-16V-25V-10-X7R-Chip-Capacitor-MLCC/1502554_32924877777.html" H 3450 1450 50  0001 C CNN
	1    3450 1450
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:D_Zener_Small_ALT-3Phase-rescue D2
U 1 1 5BDBB72B
P 3750 1250
F 0 "D2" H 3750 1340 50  0000 C CNN
F 1 "BZT52C15S" H 3750 1160 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-323" V 3750 1250 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100-PCS-LOT-BZT52C15S-BZT52C15-SOD-323-123-WJ/4580010_32950079051.html" V 3750 1250 50  0001 C CNN
	1    3750 1250
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:D_Small_ALT-3Phase-rescue D1
U 1 1 5BDBDB31
P 3350 1700
F 0 "D1" H 3300 1780 50  0000 L CNN
F 1 "ES1J" H 3200 1620 50  0001 L CNN
F 2 "Diode_SMD:D_SMA" V 3350 1700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-SS14-In5819-1n5819-SS16-SS24-SS34-IN5822-SS36-sr360-SS110-SS220-SS310-ES1J-SMD-SMA/630494_32865102060.html" H 3350 1700 50  0001 C CNN
	1    3350 1700
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:CP_Small-3Phase-rescue C12
U 1 1 5BDBE803
P 4000 1700
F 0 "C12" H 4010 1770 50  0000 L CNN
F 1 "47uF 25v" H 4010 1620 50  0001 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4000 1700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-lot-47uF-25V-Electrolytic-Capacitor-5x7mm-Aluminum-Electrolytic-Capacitors-Free-Shipping/1743026_32515722504.html" H 4000 1700 50  0001 C CNN
	1    4000 1700
	1    0    0    -1  
$EndComp
Text GLabel 4150 1300 1    60   Input ~ 0
15v
$Comp
L 3Phase-rescue:D_Small_ALT-3Phase-rescue D3
U 1 1 5BDBD504
P 4000 1400
F 0 "D3" H 3950 1480 50  0000 L CNN
F 1 "ES1J" H 3850 1320 50  0001 L CNN
F 2 "Diode_SMD:D_SMA" V 4000 1400 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-SS14-In5819-1n5819-SS16-SS24-SS34-IN5822-SS36-sr360-SS110-SS220-SS310-ES1J-SMD-SMA/630494_32865102060.html" V 4000 1400 50  0001 C CNN
	1    4000 1400
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:SY8113-3Phase-rescue U10
U 1 1 5BDCEBDA
P 4600 1450
F 0 "U10" H 4450 1675 50  0000 R CNN
F 1 "SY8113" H 4475 1550 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-6" H 4600 875 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-SY8113ADC-SY8113A-SY8113-SOT23-6/614856_32774281809.html" H 4600 1450 50  0001 C CNN
	1    4600 1450
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C17
U 1 1 5BDCF00F
P 4150 1700
F 0 "C17" H 4160 1770 50  0000 L CNN
F 1 "100nF 50v" H 4160 1620 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4150 1700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 4150 1700 50  0001 C CNN
	1    4150 1700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:L-3Phase-rescue L4
U 1 1 5BDD2082
P 5275 1350
F 0 "L4" V 5225 1350 50  0000 C CNN
F 1 "2.2uH" V 5350 1350 50  0000 C CNN
F 2 "Footprints:CD32_3.5x3.0" H 5275 1350 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100Pcs-CD32-Chip-Power-Inductance-3521-3-5-3-2mm-inductor-2-2uH-3-3uH-4/210378_32858183800.html" H 5275 1350 50  0001 C CNN
	1    5275 1350
	0    -1   -1   0   
$EndComp
Text GLabel 825  10650 0    60   Input ~ 0
IN_L3
$Comp
L 3Phase-rescue:D_ALT-3Phase-rescue D6
U 1 1 5BDDF7B6
P 1450 1750
F 0 "D6" H 1550 1800 50  0000 C CNN
F 1 "1N4007" H 1450 1650 50  0001 C CNN
F 2 "Diode_SMD:D_SMA" H 1450 1750 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-Diode-1N4001-M1-1N4005-M5-1N4007-M7-1N4004-M4-SR160-1N4148-1N4148-1N5819-RS1M-RS2M/730231_32868492349.html" H 1450 1750 50  0001 C CNN
	1    1450 1750
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:D_ALT-3Phase-rescue D4
U 1 1 5BDDF9F7
P 1450 1150
F 0 "D4" H 1550 1200 50  0000 C CNN
F 1 "1N4007" H 1450 1050 50  0001 C CNN
F 2 "Diode_SMD:D_SMA" H 1450 1150 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-Diode-1N4001-M1-1N4005-M5-1N4007-M7-1N4004-M4-SR160-1N4148-1N4148-1N5819-RS1M-RS2M/730231_32868492349.html" H 1450 1150 50  0001 C CNN
	1    1450 1150
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:D_ALT-3Phase-rescue D5
U 1 1 5BDDFAD0
P 1450 1450
F 0 "D5" H 1550 1500 50  0000 C CNN
F 1 "1N4007" H 1450 1350 50  0001 C CNN
F 2 "Diode_SMD:D_SMA" H 1450 1450 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-Diode-1N4001-M1-1N4005-M5-1N4007-M7-1N4004-M4-SR160-1N4148-1N4148-1N5819-RS1M-RS2M/730231_32868492349.html" H 1450 1450 50  0001 C CNN
	1    1450 1450
	-1   0    0    1   
$EndComp
Text GLabel 1300 1000 2    60   Input ~ 0
IN_L3
Text GLabel 1300 1300 2    60   Input ~ 0
IN_L2
Text GLabel 1300 1600 2    60   Input ~ 0
IN_L1
$Comp
L 3Phase-rescue:CP_Small-3Phase-rescue C18
U 1 1 5BDECD27
P 5525 1700
F 0 "C18" H 5535 1770 50  0000 L CNN
F 1 "100uF 16v" H 5535 1620 50  0001 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5525 1700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/50pcs-Electrolytic-Capacitor-100uF-16V-5-7mm-Aluminum-Electrolytic-Capacitor-5X7mm/1361740_32834736874.html" H 5525 1700 50  0001 C CNN
	1    5525 1700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:LTC4054-3Phase-rescue U12
U 1 1 5BDEE248
P 6050 1450
F 0 "U12" H 5900 1675 50  0000 R CNN
F 1 "LTC4054" H 6150 1675 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6050 875 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/50PCS-LTC4054ES5-4-2-SOT23-5-LTC4054ES5-SOT23-LTC4054-SOT-SMD-free-shipping/1911309_32713985397.html" H 6050 1450 50  0001 C CNN
	1    6050 1450
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:Battery_Cell-3Phase-rescue BT1
U 1 1 5BDEE828
P 6700 1700
F 0 "BT1" H 6800 1800 50  0000 L CNN
F 1 "Battery_Cell" H 6800 1700 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" V 6700 1760 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100-Sets-Micro-JST-2-0-PH-2-Pin-Connector-plug-Male-Female-Crimps/1682443_32772593322.html" H 6700 1760 50  0001 C CNN
	1    6700 1700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:Q_PMOS_GSD-3Phase-rescue Q1
U 1 1 5BDF3EC9
P 6600 1100
F 0 "Q1" H 6800 1150 50  0000 L CNN
F 1 "Si2301DS" H 6800 1050 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6800 1200 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-SI2300DS-SI2301DS-SI2302DS-SI2303DS-SI2304DS-SI2305DS-SI2306DS-SI2307DS-SI2308DS-SI2309DS-SOT-23-new-MOS-FET/1949429_32908390991.html" H 6600 1100 50  0001 C CNN
	1    6600 1100
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:D_Schottky_Small_ALT-3Phase-rescue D8
U 1 1 5BDF4375
P 6525 850
F 0 "D8" H 6475 930 50  0000 L CNN
F 1 "SS14" H 6245 770 50  0001 L CNN
F 2 "Diode_SMD:D_SMA" V 6525 850 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-SMA-Rectifier-diode-1N5819-SS14-1N5822-SS34-SR160-SS16-1N5817-SS12-SR1100-SS110-SR360-SS36/1969251_32947185076.html" V 6525 850 50  0001 C CNN
	1    6525 850 
	-1   0    0    1   
$EndComp
Text GLabel 5900 1975 0    60   Input ~ 0
CHRG
$Comp
L 3Phase-rescue:PS7516-3Phase-rescue U7
U 1 1 5BE18D52
P 4625 9000
F 0 "U7" H 4825 8750 50  0000 C CNN
F 1 "PS7516" H 4525 8750 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-6" H 4625 9100 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/MP1470GJ-LF-Z-SY8008CAAC-CL6808-BCP53-16T1G-L78L33ACUTR-BF998R-PS7516-CAT6219-330TD-GT3-MT7201C-TMB12A05/1913237_32909344731.html" H 4625 9100 50  0001 C CNN
	1    4625 9000
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue P1
U 1 1 5BE1F723
P 850 1700
F 0 "P1" H 850 1950 50  0000 C CNN
F 1 "CONN_01X04" V 950 1700 50  0000 C CNN
F 2 "Footprints:PhoenixContact_MC_1,5_4-G-3.81_1x04_P3.81mm_Horizontal" H 850 1700 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/20pair-3-81-4pin-Right-angle-Terminal-plug-type-300V-8A-3-81mm-pitch-connector-pcb/725808_32921123410.html" H 850 1700 50  0001 C CNN
	1    850  1700
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R57
U 1 1 5BEA6C0B
P 7100 1600
F 0 "R57" V 7180 1600 50  0000 C CNN
F 1 "270k" H 7100 1600 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7030 1600 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 7100 1600 50  0001 C CNN
	1    7100 1600
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R56
U 1 1 5BEA6F94
P 6900 1350
F 0 "R56" V 6980 1350 50  0000 C CNN
F 1 "100k" H 6900 1350 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6830 1350 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 6900 1350 50  0001 C CNN
	1    6900 1350
	0    1    1    0   
$EndComp
Text GLabel 7150 1350 2    60   Input ~ 0
BAT_ADC
Text GLabel 4900 1975 0    60   Input ~ 0
EXT
Text GLabel 7150 850  2    60   Input ~ 0
POWER
Text GLabel 4125 9450 0    60   Input ~ 0
POWER
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C13
U 1 1 5BEB8358
P 4175 9650
F 0 "C13" H 4185 9720 50  0000 L CNN
F 1 "100nF" H 4185 9570 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4175 9650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 4175 9650 50  0001 C CNN
	1    4175 9650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5BEB8531
P 3900 9800
F 0 "#PWR012" H 3900 9550 50  0001 C CNN
F 1 "GND" H 3900 9650 50  0000 C CNN
F 2 "" H 3900 9800 50  0000 C CNN
F 3 "" H 3900 9800 50  0000 C CNN
	1    3900 9800
	0    1    1    0   
$EndComp
Text GLabel 4150 9000 0    60   Input ~ 0
POWER_EN
$Comp
L 3Phase-rescue:L-3Phase-rescue L3
U 1 1 5BEC97FD
P 4525 8650
F 0 "L3" V 4475 8650 50  0000 C CNN
F 1 "10uH" V 4600 8650 50  0000 C CNN
F 2 "Footprints:CD43_4.5x4.0" H 4525 8650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/CD43-10UH-100-1A-winding-chip-power-inductors-100pieces/3006032_32811658266.html" H 4525 8650 50  0001 C CNN
	1    4525 8650
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R50
U 1 1 5BEC99BD
P 5200 9275
F 0 "R50" V 5280 9275 50  0000 C CNN
F 1 "56k" V 5200 9275 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5130 9275 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 5200 9275 50  0001 C CNN
	1    5200 9275
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R49
U 1 1 5BEC9B5A
P 5200 8850
F 0 "R49" V 5280 8850 50  0000 C CNN
F 1 "300k" V 5200 8850 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5130 8850 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 5200 8850 50  0001 C CNN
	1    5200 8850
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C14
U 1 1 5BEC9CD0
P 5425 9575
F 0 "C14" H 5435 9645 50  0000 L CNN
F 1 "100nF" H 5435 9495 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5425 9575 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 5425 9575 50  0001 C CNN
	1    5425 9575
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CP_Small-3Phase-rescue C15
U 1 1 5BEC9E75
P 5325 9025
F 0 "C15" H 5335 9095 50  0000 L CNN
F 1 "100uF 16v" H 5335 8945 50  0001 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5325 9025 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-lot-Electrolytic-Capacitors-10V-100uf-10V-Electrolytic-Capacitor-size-4mm-7mm/1021664_1481491846.html" H 5325 9025 50  0001 C CNN
	1    5325 9025
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR041
U 1 1 5BD43251
P 6975 7400
F 0 "#PWR041" H 6975 7250 50  0001 C CNN
F 1 "+5V" V 6900 7475 50  0000 C CNN
F 2 "" H 6975 7400 50  0000 C CNN
F 3 "" H 6975 7400 50  0000 C CNN
	1    6975 7400
	0    -1   1    0   
$EndComp
$Comp
L Components:MCP6004 U11
U 1 1 5BEDF329
P 2225 9450
F 0 "U11" H 2225 9650 50  0000 L CNN
F 1 "MCP6004" H 2175 9250 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2175 9550 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 2275 9650 50  0001 C CNN
	1    2225 9450
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R51
U 1 1 5BEE0E5D
P 6725 9050
F 0 "R51" V 6805 9050 50  0000 C CNN
F 1 "10k" V 6725 9050 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6655 9050 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 6725 9050 50  0001 C CNN
	1    6725 9050
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R52
U 1 1 5BEE103D
P 6600 9600
F 0 "R52" V 6680 9600 50  0000 C CNN
F 1 "10k" V 6600 9600 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 9600 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-8-2K-8K2-9-1K-9K1/624710_32949773329.html" H 6600 9600 50  0001 C CNN
	1    6600 9600
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C16
U 1 1 5BEE335C
P 6725 9400
F 0 "C16" H 6735 9470 50  0000 L CNN
F 1 "100nF" H 6735 9320 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6725 9400 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 6725 9400 50  0001 C CNN
	1    6725 9400
	1    0    0    -1  
$EndComp
$Comp
L Components:+1V65 #PWR047
U 1 1 5BEEB00A
P 7600 9350
F 0 "#PWR047" H 7600 9200 50  0001 C CNN
F 1 "+1V65" V 7525 9350 50  0000 C CNN
F 2 "" H 7600 9350 50  0000 C CNN
F 3 "" H 7600 9350 50  0000 C CNN
	1    7600 9350
	0    1    1    0   
$EndComp
$Comp
L power:+3.3VA #PWR046
U 1 1 5BEED2DB
P 7300 8850
F 0 "#PWR046" H 7300 8700 50  0001 C CNN
F 1 "+3.3VA" V 7300 9100 50  0000 C CNN
F 2 "" H 7300 8850 50  0000 C CNN
F 3 "" H 7300 8850 50  0000 C CNN
	1    7300 8850
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR045
U 1 1 5BEED445
P 7325 8600
F 0 "#PWR045" H 7325 8450 50  0001 C CNN
F 1 "+5V" V 7325 8800 50  0000 C CNN
F 2 "" H 7325 8600 50  0000 C CNN
F 3 "" H 7325 8600 50  0000 C CNN
	1    7325 8600
	0    1    1    0   
$EndComp
Text GLabel 825  10000 0    60   Input ~ 0
IN_L2
Text GLabel 825  9350 0    60   Input ~ 0
IN_L1
Text GLabel 4375 2825 0    60   Input ~ 0
L1I_ADC
Text GLabel 4100 4775 0    60   Input ~ 0
L2I_ADC
Text GLabel 4100 6475 0    60   Input ~ 0
L3I_ADC
Text GLabel 4150 7600 0    60   Input ~ 0
NI_ADC
$Comp
L Components:LED_Dual_ACA D12
U 1 1 5BFAD7FE
P 2300 5225
F 0 "D12" H 2025 5175 50  0000 C CNN
F 1 "3mm bicolor LED" H 2300 4975 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 2300 5225 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-LED-3mm-Round-Diffused-Red-Green-two-Color-Common-Anode-cathode-LED-Diode-Light-Emitting/1758868_32887904538.html" H 2300 5225 50  0001 C CNN
	1    2300 5225
	-1   0    0    1   
$EndComp
$Comp
L Components:LED_Dual_ACA D10
U 1 1 5BFADB33
P 2300 5675
F 0 "D10" H 2075 5550 50  0000 C CNN
F 1 "3mm bicolor LED" H 2300 5425 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 2300 5675 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-LED-3mm-Round-Diffused-Red-Green-two-Color-Common-Anode-cathode-LED-Diode-Light-Emitting/1758868_32887904538.html" H 2300 5675 50  0001 C CNN
	1    2300 5675
	-1   0    0    1   
$EndComp
$Comp
L Components:LED_Dual_ACA D9
U 1 1 5BFADE7C
P 2300 6125
F 0 "D9" H 2075 6050 50  0000 C CNN
F 1 "3mm bicolor LED" H 2300 5875 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 2300 6125 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-LED-3mm-Round-Diffused-Red-Green-two-Color-Common-Anode-cathode-LED-Diode-Light-Emitting/1758868_32887904538.html" H 2300 6125 50  0001 C CNN
	1    2300 6125
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:LED_ALT-3Phase-rescue D11
U 1 1 5BFADFE9
P 2375 6425
F 0 "D11" H 2375 6525 50  0000 C CNN
F 1 "3mm blue LED" H 2375 6325 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 2375 6425 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-x-Long-Leg-3mm-5mm-White-Diffused-Jade-Green-Yellow-Red-Blue-Ultra-Bright-LED/102034_32887271791.html" H 2375 6425 50  0001 C CNN
	1    2375 6425
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:Buzzer-3Phase-rescue BZ1
U 1 1 5BFAE180
P 2175 3050
F 0 "BZ1" V 2050 2975 50  0000 L CNN
F 1 "HMB1275-05B" H 2325 3000 50  0001 L CNN
F 2 "Footprints:Buzzer_9x5.5RM4" V 2150 3150 50  0001 C CNN
F 3 "" V 2150 3150 50  0000 C CNN
	1    2175 3050
	0    -1   -1   0   
$EndComp
$Comp
L Components:NanoPi P2
U 1 1 5BFAFAB2
P 2350 7675
F 0 "P2" H 2700 7750 50  0000 C CNN
F 1 "NanoPI AIR" H 2450 7675 50  0001 C CNN
F 2 "Footprints:NanoPI Air" H 2350 7675 50  0001 C CNN
F 3 "" H 2350 7675 50  0000 C CNN
	1    2350 7675
	1    0    0    -1  
$EndComp
Text GLabel 2200 7175 2    60   Input ~ 0
L1I_ADC
Text GLabel 1800 7075 2    60   Input ~ 0
L2I_ADC
Text GLabel 2200 6975 2    60   Input ~ 0
L3I_ADC
$Comp
L Components:MCP6004 U11
U 2 1 5BFE63CA
P 7100 9350
F 0 "U11" H 7100 9550 50  0000 L CNN
F 1 "MCP6004" H 7050 9150 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 7050 9450 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 7150 9550 50  0001 C CNN
	2    7100 9350
	1    0    0    -1  
$EndComp
$Comp
L Components:MCP6004 U11
U 3 1 5BFEB564
P 2225 10750
F 0 "U11" H 2225 10950 50  0000 L CNN
F 1 "MCP6004" H 2175 10550 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2175 10850 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 2275 10950 50  0001 C CNN
	3    2225 10750
	1    0    0    -1  
$EndComp
$Comp
L Components:MCP6004 U11
U 4 1 5BFEBAD0
P 2225 10100
F 0 "U11" H 2225 10300 50  0000 L CNN
F 1 "MCP6004" H 2175 9900 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2175 10200 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 2275 10300 50  0001 C CNN
	4    2225 10100
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R62
U 1 1 5BFFA9B0
P 2725 9450
F 0 "R62" V 2805 9450 50  0000 C CNN
F 1 "100h" V 2725 9450 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2655 9450 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 2725 9450 50  0001 C CNN
	1    2725 9450
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C19
U 1 1 5BFFE182
P 2900 9575
F 0 "C19" H 2910 9645 50  0000 L CNN
F 1 "100nF" H 2910 9495 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2900 9575 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 2900 9575 50  0001 C CNN
	1    2900 9575
	1    0    0    -1  
$EndComp
Text GLabel 3000 9450 2    60   Input ~ 0
L1V_ADC
$Comp
L power:GND #PWR08
U 1 1 5C00A67E
P 2900 9700
F 0 "#PWR08" H 2900 9450 50  0001 C CNN
F 1 "GND" H 2900 9550 50  0000 C CNN
F 2 "" H 2900 9700 50  0000 C CNN
F 3 "" H 2900 9700 50  0000 C CNN
	1    2900 9700
	1    0    0    -1  
$EndComp
$Comp
L Components:+1V65 #PWR01
U 1 1 5C00E7F5
P 1550 9175
F 0 "#PWR01" H 1550 9025 50  0001 C CNN
F 1 "+1V65" V 1550 9425 50  0000 C CNN
F 2 "" H 1550 9175 50  0000 C CNN
F 3 "" H 1550 9175 50  0000 C CNN
	1    1550 9175
	0    -1   -1   0   
$EndComp
$Comp
L Components:+1V65 #PWR02
U 1 1 5C015D32
P 1550 9825
F 0 "#PWR02" H 1550 9675 50  0001 C CNN
F 1 "+1V65" V 1550 10075 50  0000 C CNN
F 2 "" H 1550 9825 50  0000 C CNN
F 3 "" H 1550 9825 50  0000 C CNN
	1    1550 9825
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R63
U 1 1 5C018829
P 2725 10100
F 0 "R63" V 2805 10100 50  0000 C CNN
F 1 "100h" V 2725 10100 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2655 10100 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 2725 10100 50  0001 C CNN
	1    2725 10100
	0    1    1    0   
$EndComp
Text GLabel 3000 10100 2    60   Input ~ 0
L2V_ADC
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C20
U 1 1 5C018F55
P 2900 10225
F 0 "C20" H 2910 10295 50  0000 L CNN
F 1 "100nF" H 2910 10145 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2900 10225 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 2900 10225 50  0001 C CNN
	1    2900 10225
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5C01912D
P 2900 10350
F 0 "#PWR09" H 2900 10100 50  0001 C CNN
F 1 "GND" H 2900 10200 50  0000 C CNN
F 2 "" H 2900 10350 50  0000 C CNN
F 3 "" H 2900 10350 50  0000 C CNN
	1    2900 10350
	1    0    0    -1  
$EndComp
$Comp
L Components:+1V65 #PWR03
U 1 1 5C01CBFF
P 1550 10475
F 0 "#PWR03" H 1550 10325 50  0001 C CNN
F 1 "+1V65" V 1550 10725 50  0000 C CNN
F 2 "" H 1550 10475 50  0000 C CNN
F 3 "" H 1550 10475 50  0000 C CNN
	1    1550 10475
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R64
U 1 1 5C01E49B
P 2725 10750
F 0 "R64" V 2805 10750 50  0000 C CNN
F 1 "100h" V 2725 10750 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2655 10750 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 2725 10750 50  0001 C CNN
	1    2725 10750
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C21
U 1 1 5C01E678
P 2900 10875
F 0 "C21" H 2910 10945 50  0000 L CNN
F 1 "100nF" H 2910 10795 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2900 10875 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 2900 10875 50  0001 C CNN
	1    2900 10875
	1    0    0    -1  
$EndComp
Text GLabel 3000 10750 2    60   Input ~ 0
L3V_ADC
$Comp
L power:GND #PWR010
U 1 1 5C020156
P 2900 11000
F 0 "#PWR010" H 2900 10750 50  0001 C CNN
F 1 "GND" H 2900 10850 50  0000 C CNN
F 2 "" H 2900 11000 50  0000 C CNN
F 3 "" H 2900 11000 50  0000 C CNN
	1    2900 11000
	1    0    0    -1  
$EndComp
$Comp
L Components:STM32F042 U13
U 1 1 5C0F1416
P 1300 8325
F 0 "U13" H 1300 7925 60  0000 C CNN
F 1 "STM32F042" H 1250 7800 60  0000 C CNN
F 2 "Housings_QFP:LQFP-32_7x7mm_Pitch0.8mm" H 1300 8325 60  0001 C CNN
F 3 "" H 1300 8325 60  0001 C CNN
	1    1300 8325
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C0F1AED
P 1675 8850
F 0 "#PWR04" H 1675 8600 50  0001 C CNN
F 1 "GND" H 1675 8700 50  0000 C CNN
F 2 "" H 1675 8850 50  0000 C CNN
F 3 "" H 1675 8850 50  0000 C CNN
	1    1675 8850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3VA #PWR05
U 1 1 5C0F1C96
P 2325 8525
F 0 "#PWR05" H 2325 8375 50  0001 C CNN
F 1 "+3.3VA" V 2325 8775 50  0000 C CNN
F 2 "" H 2325 8525 50  0000 C CNN
F 3 "" H 2325 8525 50  0000 C CNN
	1    2325 8525
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C22
U 1 1 5C10FD29
P 1800 8675
F 0 "C22" H 1810 8745 50  0000 L CNN
F 1 "100nF" H 1810 8595 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1800 8675 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 1800 8675 50  0001 C CNN
	1    1800 8675
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C23
U 1 1 5C10FF28
P 2000 8675
F 0 "C23" H 2010 8745 50  0000 L CNN
F 1 "100nF" H 2010 8595 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2000 8675 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 2000 8675 50  0001 C CNN
	1    2000 8675
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C24
U 1 1 5C110430
P 2200 8675
F 0 "C24" H 2210 8745 50  0000 L CNN
F 1 "C_Small" H 2210 8595 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2200 8675 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 2200 8675 50  0001 C CNN
	1    2200 8675
	1    0    0    -1  
$EndComp
Wire Wire Line
	4375 2825 4475 2825
Wire Wire Line
	4500 2825 4475 2825
Connection ~ 4475 2825
Wire Notes Line
	3550 8500 7775 8500
Wire Notes Line
	3550 9100 475  9100
Wire Notes Line
	3550 2525 3550 9975
Wire Wire Line
	2900 11000 2900 10975
Connection ~ 2550 10750
Connection ~ 2900 10750
Wire Wire Line
	2900 10775 2900 10750
Wire Wire Line
	2875 10750 2900 10750
Wire Wire Line
	2525 10750 2550 10750
Wire Wire Line
	2550 11000 2550 10750
Wire Wire Line
	1900 11000 2550 11000
Wire Wire Line
	1900 10850 1900 11000
Wire Wire Line
	1925 10850 1900 10850
Wire Wire Line
	1550 10475 1575 10475
Connection ~ 1900 10650
Wire Wire Line
	1900 10475 1900 10650
Wire Wire Line
	1875 10475 1900 10475
Wire Wire Line
	2900 10325 2900 10350
Connection ~ 2900 10100
Wire Wire Line
	2900 10125 2900 10100
Wire Wire Line
	2875 10100 2900 10100
Connection ~ 2550 10100
Wire Wire Line
	2525 10100 2550 10100
Wire Wire Line
	2550 10350 2550 10100
Wire Wire Line
	1900 10350 2550 10350
Wire Wire Line
	1900 10200 1900 10350
Wire Wire Line
	1925 10200 1900 10200
Wire Wire Line
	1550 9825 1575 9825
Connection ~ 1900 10000
Wire Wire Line
	1900 9825 1875 9825
Wire Wire Line
	1900 10000 1900 9825
Wire Wire Line
	1875 10000 1900 10000
Wire Wire Line
	1575 9175 1550 9175
Connection ~ 2900 9450
Wire Wire Line
	2900 9450 2900 9475
Wire Wire Line
	2875 9450 2900 9450
Connection ~ 2550 9450
Connection ~ 1900 9350
Wire Wire Line
	1900 9175 1900 9350
Wire Wire Line
	1900 9175 1875 9175
Wire Wire Line
	1875 9350 1900 9350
Wire Wire Line
	2525 9450 2550 9450
Wire Wire Line
	2550 9700 2550 9450
Wire Wire Line
	1900 9700 2550 9700
Wire Wire Line
	1900 9550 1900 9700
Wire Wire Line
	1925 9550 1900 9550
Wire Wire Line
	825  10650 875  10650
Wire Wire Line
	875  10000 825  10000
Wire Wire Line
	825  9350 875  9350
Wire Wire Line
	5625 7700 5650 7700
Wire Wire Line
	5000 7300 5000 7275
Wire Wire Line
	5625 6575 5650 6575
Wire Wire Line
	6275 7600 6625 7600
Wire Wire Line
	4475 3125 4475 3150
Wire Wire Line
	4575 5200 4575 5225
Wire Wire Line
	5625 4875 5650 4875
Wire Wire Line
	4875 3200 4875 3275
Wire Wire Line
	5650 3250 5650 3275
Wire Wire Line
	2000 1150 2050 1150
Connection ~ 4200 7600
Connection ~ 4175 6475
Connection ~ 7425 9350
Wire Wire Line
	7400 9350 7425 9350
Wire Wire Line
	7425 9725 7425 9350
Wire Wire Line
	6775 9725 7425 9725
Wire Wire Line
	6775 9450 6775 9725
Wire Wire Line
	6800 9450 6775 9450
Connection ~ 6725 9250
Connection ~ 6725 9800
Wire Wire Line
	6600 9250 6725 9250
Wire Wire Line
	6725 9200 6725 9250
Connection ~ 6725 8850
Wire Wire Line
	6600 9250 6600 9450
Connection ~ 6600 9800
Wire Wire Line
	6600 9800 6600 9750
Wire Wire Line
	6725 9800 6725 9500
Connection ~ 5425 9800
Connection ~ 4175 9450
Wire Wire Line
	4175 8650 4175 8900
Connection ~ 5200 9800
Wire Wire Line
	5200 9800 5200 9425
Connection ~ 5200 9100
Wire Wire Line
	5200 9000 5200 9100
Connection ~ 5200 8600
Wire Wire Line
	5200 8700 5200 8600
Connection ~ 5325 9800
Wire Wire Line
	5425 9800 5425 9675
Wire Wire Line
	5325 9800 5325 9125
Connection ~ 5325 8600
Wire Wire Line
	5325 8600 5325 8925
Connection ~ 5425 8600
Wire Wire Line
	5425 8600 5425 9475
Wire Wire Line
	5075 8600 5200 8600
Wire Wire Line
	5075 9000 5075 8600
Wire Wire Line
	4925 9000 5075 9000
Wire Wire Line
	4975 8650 4675 8650
Wire Wire Line
	4975 8900 4975 8650
Wire Wire Line
	4925 8900 4975 8900
Wire Wire Line
	4375 8650 4175 8650
Connection ~ 4175 9800
Wire Wire Line
	4175 9800 4175 9750
Connection ~ 4275 9800
Wire Wire Line
	4275 9100 4325 9100
Wire Wire Line
	4275 9800 4275 9100
Connection ~ 6700 850 
Connection ~ 7100 1350
Wire Wire Line
	6350 1350 6700 1350
Wire Wire Line
	7100 1350 7100 1450
Wire Wire Line
	7050 1350 7100 1350
Connection ~ 6700 1850
Wire Wire Line
	7100 1850 7100 1750
Wire Wire Line
	6500 1500 6500 1450
Wire Wire Line
	3600 1550 3600 1500
Connection ~ 2650 1300
Connection ~ 2650 1350
Wire Wire Line
	2650 1350 2700 1350
Connection ~ 2650 1450
Wire Wire Line
	2650 1450 2700 1450
Wire Wire Line
	2650 1250 2650 1300
Wire Wire Line
	2650 1550 2700 1550
Connection ~ 2450 1300
Wire Wire Line
	2450 1300 2450 1400
Wire Wire Line
	2650 1250 2700 1250
Wire Wire Line
	2400 1300 2450 1300
Connection ~ 2050 1300
Wire Wire Line
	2050 1150 2050 1300
Wire Wire Line
	1800 1300 1800 1400
Wire Wire Line
	1800 1300 2050 1300
Connection ~ 1650 1450
Wire Wire Line
	1600 1450 1650 1450
Wire Wire Line
	1650 1750 1600 1750
Wire Wire Line
	1150 1550 1050 1550
Wire Wire Line
	1200 1650 1050 1650
Connection ~ 5700 1350
Wire Wire Line
	5700 1100 5700 1350
Connection ~ 6700 1350
Wire Wire Line
	6400 1975 5900 1975
Wire Wire Line
	6400 1550 6400 1975
Wire Wire Line
	6350 1550 6400 1550
Wire Wire Line
	6700 850  6700 900 
Wire Wire Line
	6625 850  6700 850 
Wire Wire Line
	6425 850  6350 850 
Connection ~ 6350 1100
Wire Wire Line
	5700 1100 6350 1100
Connection ~ 6500 1850
Wire Wire Line
	6500 1850 6500 1800
Wire Wire Line
	1050 1850 1800 1850
Wire Wire Line
	6500 1450 6350 1450
Wire Wire Line
	6700 1300 6700 1350
Connection ~ 6050 1850
Wire Wire Line
	6700 1850 6700 1800
Connection ~ 5525 1850
Wire Wire Line
	6050 1850 6050 1700
Wire Wire Line
	5525 1850 5525 1800
Connection ~ 4150 1350
Connection ~ 1250 1750
Wire Wire Line
	1250 1600 1250 1750
Wire Wire Line
	1300 1600 1250 1600
Connection ~ 1200 1450
Wire Wire Line
	1200 1300 1200 1450
Wire Wire Line
	1300 1300 1200 1300
Connection ~ 1150 1150
Wire Wire Line
	1150 1000 1150 1150
Wire Wire Line
	1300 1000 1150 1000
Wire Wire Line
	1050 1750 1250 1750
Wire Wire Line
	1150 1150 1300 1150
Wire Wire Line
	1200 1450 1300 1450
Connection ~ 1650 1150
Wire Wire Line
	1650 1150 1650 1450
Wire Wire Line
	1600 1150 1650 1150
Connection ~ 1800 1850
Connection ~ 3350 1850
Wire Wire Line
	3350 1800 3350 1850
Connection ~ 5000 1850
Connection ~ 4600 1850
Wire Wire Line
	5000 1850 5000 1800
Wire Wire Line
	5000 1500 5000 1450
Wire Wire Line
	4900 1150 4800 1150
Wire Wire Line
	4900 1350 4900 1150
Wire Wire Line
	4600 1200 4600 1150
Wire Wire Line
	4600 1850 4600 1700
Wire Wire Line
	4150 1350 4300 1350
Connection ~ 4150 1550
Wire Wire Line
	4150 1300 4150 1350
Connection ~ 4150 1850
Wire Wire Line
	4150 1850 4150 1800
Connection ~ 4000 1850
Connection ~ 4000 1250
Wire Wire Line
	4000 1150 4000 1250
Wire Wire Line
	3850 1250 4000 1250
Connection ~ 3450 1350
Wire Wire Line
	3450 1350 3450 1150
Connection ~ 3600 1250
Wire Wire Line
	3600 1250 3600 1300
Wire Wire Line
	3300 1250 3600 1250
Wire Wire Line
	3300 1350 3450 1350
Wire Wire Line
	3950 1550 4000 1550
Connection ~ 2050 1850
Wire Wire Line
	1800 1850 1800 1700
Connection ~ 2450 1850
Wire Wire Line
	2050 1850 2050 1700
Wire Wire Line
	2450 1850 2450 1700
Wire Wire Line
	4000 1850 4000 1800
Connection ~ 4000 1550
Wire Wire Line
	4000 1500 4000 1550
Connection ~ 3450 1550
Connection ~ 3600 1550
Connection ~ 3350 1550
Wire Wire Line
	3300 1450 3350 1450
Wire Wire Line
	3350 1450 3350 1550
Wire Wire Line
	3300 1550 3350 1550
Connection ~ 6625 8150
Connection ~ 6625 6975
Wire Wire Line
	6625 4775 6625 4850
Wire Wire Line
	6775 3175 6775 3275
Wire Wire Line
	1875 10650 1900 10650
Wire Wire Line
	1575 10650 1525 10650
Wire Wire Line
	1525 10000 1575 10000
Wire Wire Line
	1525 9350 1575 9350
Wire Wire Line
	1175 9350 1225 9350
Wire Wire Line
	1175 10000 1225 10000
Wire Wire Line
	1175 10650 1225 10650
Wire Wire Line
	7000 7400 6975 7400
Wire Wire Line
	6950 7700 6975 7700
Wire Wire Line
	6625 8150 6600 8150
Wire Wire Line
	6625 7925 6625 8150
Wire Wire Line
	4200 7925 4200 7875
Wire Wire Line
	4225 7600 4200 7600
Wire Wire Line
	4150 7600 4200 7600
Connection ~ 6275 8150
Wire Wire Line
	6275 8150 6275 7800
Wire Wire Line
	6000 8150 6275 8150
Connection ~ 5650 7700
Wire Wire Line
	5650 7700 5675 7700
Connection ~ 5225 7700
Wire Wire Line
	5200 7700 5225 7700
Wire Wire Line
	5225 8150 5225 7700
Wire Wire Line
	4175 6750 4175 6800
Wire Wire Line
	6625 6975 6600 6975
Wire Wire Line
	7000 6575 7025 6575
Connection ~ 6275 6975
Wire Wire Line
	6000 6975 6275 6975
Wire Wire Line
	6275 6675 6275 6975
Connection ~ 5650 6575
Wire Wire Line
	4150 5100 4150 5175
Wire Wire Line
	5650 6575 5675 6575
Wire Wire Line
	5100 6975 5225 6975
Wire Wire Line
	5225 6975 5225 6575
Wire Wire Line
	4525 6475 4575 6475
Wire Wire Line
	4225 6475 4175 6475
Wire Wire Line
	4100 6475 4175 6475
Wire Wire Line
	6975 4875 7000 4875
Connection ~ 6275 5275
Wire Wire Line
	6275 4975 6275 5275
Wire Wire Line
	6000 5275 6275 5275
Connection ~ 5650 4875
Wire Wire Line
	5650 4875 5675 4875
Wire Wire Line
	5200 4875 5225 4875
Connection ~ 4575 4775
Wire Wire Line
	4575 4775 4575 4900
Wire Wire Line
	4100 4775 4150 4775
Wire Wire Line
	4500 4775 4575 4775
Wire Wire Line
	6625 3675 6600 3675
Wire Wire Line
	6975 2975 7000 2975
Wire Wire Line
	6950 3275 6975 3275
Wire Wire Line
	5500 2925 5525 2925
Wire Wire Line
	5650 2925 5650 2950
Connection ~ 4875 2825
Wire Wire Line
	6000 3675 6275 3675
Connection ~ 6275 3675
Wire Wire Line
	4875 2825 4875 2900
Wire Wire Line
	4800 2825 4875 2825
Connection ~ 5650 3275
Wire Wire Line
	6275 3375 6275 3675
Wire Wire Line
	5650 3275 5675 3275
Wire Wire Line
	5525 3275 5525 2925
Connection ~ 5525 2925
Wire Wire Line
	1650 8325 2200 8325
Wire Wire Line
	1650 8625 1675 8625
Wire Wire Line
	1650 8425 2000 8425
Wire Wire Line
	1800 8825 1800 8775
Wire Wire Line
	1675 8825 1800 8825
Connection ~ 1675 8825
Wire Wire Line
	2000 8775 2000 8825
Connection ~ 1800 8825
Wire Wire Line
	2200 8825 2200 8775
Connection ~ 2000 8825
Wire Wire Line
	1800 8575 1800 8525
Connection ~ 1800 8525
Wire Wire Line
	2300 7925 1650 7925
Wire Wire Line
	1650 7825 2300 7825
$Comp
L 3Phase-rescue:SW_Push-3Phase-rescue SW2
U 1 1 5C13FA35
P 2225 4750
F 0 "SW2" H 2275 4850 50  0000 L CNN
F 1 "SW_Push" H 2225 4690 50  0001 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SKHH_Angled" H 2225 4950 50  0001 C CNN
F 3 "" H 2225 4950 50  0000 C CNN
	1    2225 4750
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5C1438E9
P 2475 4875
F 0 "#PWR07" H 2475 4625 50  0001 C CNN
F 1 "GND" V 2475 4650 50  0000 C CNN
F 2 "" H 2475 4875 50  0000 C CNN
F 3 "" H 2475 4875 50  0000 C CNN
	1    2475 4875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2625 6125 2600 6125
Text GLabel 1800 6875 2    60   Input ~ 0
NI_ADC
Text GLabel 1800 7475 2    60   Input ~ 0
L1V_ADC
Text GLabel 2200 7375 2    60   Input ~ 0
L2V_ADC
Text GLabel 1800 7275 2    60   Input ~ 0
L3V_ADC
Wire Wire Line
	5225 5225 5225 4875
Connection ~ 5225 4875
Wire Wire Line
	4200 4775 4150 4775
Connection ~ 4150 4775
Wire Wire Line
	5650 6300 5650 6375
Wire Wire Line
	5200 6575 5225 6575
Connection ~ 5225 6575
Connection ~ 4575 6475
Wire Wire Line
	5225 8150 5125 8150
Wire Wire Line
	4475 2825 4475 2925
Wire Wire Line
	2550 10750 2575 10750
Wire Wire Line
	2900 10750 3000 10750
Wire Wire Line
	1900 10650 1925 10650
Wire Wire Line
	2900 10100 3000 10100
Wire Wire Line
	2550 10100 2575 10100
Wire Wire Line
	1900 10000 1925 10000
Wire Wire Line
	2900 9450 3000 9450
Wire Wire Line
	2550 9450 2575 9450
Wire Wire Line
	1900 9350 1925 9350
Wire Wire Line
	5650 8150 5700 8150
Wire Wire Line
	5650 6975 5700 6975
Wire Wire Line
	5650 5275 5700 5275
Wire Wire Line
	5650 3675 5700 3675
Wire Wire Line
	4200 7600 4200 7675
Wire Wire Line
	4175 6475 4175 6550
Wire Wire Line
	7425 9350 7525 9350
Wire Wire Line
	6725 9250 6800 9250
Wire Wire Line
	6725 9250 6725 9300
Wire Wire Line
	6600 9800 6725 9800
Wire Wire Line
	5200 9800 5325 9800
Wire Wire Line
	5200 9100 5200 9125
Wire Wire Line
	5200 8600 5325 8600
Wire Wire Line
	5325 9800 5425 9800
Wire Wire Line
	5325 8600 5425 8600
Wire Wire Line
	4175 9800 4275 9800
Wire Wire Line
	6700 850  7150 850 
Wire Wire Line
	7100 1350 7150 1350
Wire Wire Line
	6700 1850 7100 1850
Wire Wire Line
	2650 1300 2650 1350
Wire Wire Line
	2650 1350 2650 1450
Wire Wire Line
	2650 1450 2650 1550
Wire Wire Line
	2450 1300 2650 1300
Wire Wire Line
	2050 1300 2050 1400
Wire Wire Line
	2050 1300 2100 1300
Wire Wire Line
	1650 1450 1650 1750
Wire Wire Line
	5700 1350 5750 1350
Wire Wire Line
	6700 1350 6750 1350
Wire Wire Line
	6700 1350 6700 1500
Wire Wire Line
	6350 1100 6400 1100
Wire Wire Line
	6500 1850 6700 1850
Wire Wire Line
	6050 1850 6500 1850
Wire Wire Line
	4150 1350 4150 1550
Wire Wire Line
	1250 1750 1300 1750
Wire Wire Line
	1200 1450 1200 1650
Wire Wire Line
	1150 1150 1150 1550
Wire Wire Line
	1650 1150 1700 1150
Wire Wire Line
	1800 1850 2050 1850
Wire Wire Line
	3350 1850 3350 1900
Wire Wire Line
	3350 1850 4000 1850
Wire Wire Line
	4600 1850 5000 1850
Wire Wire Line
	4150 1550 4275 1550
Wire Wire Line
	4150 1550 4150 1600
Wire Wire Line
	4150 1850 4275 1850
Wire Wire Line
	4000 1850 4150 1850
Wire Wire Line
	4000 1250 4000 1300
Wire Wire Line
	3600 1250 3650 1250
Wire Wire Line
	2050 1850 2450 1850
Wire Wire Line
	2450 1850 3350 1850
Wire Wire Line
	4000 1550 4150 1550
Wire Wire Line
	4000 1550 4000 1600
Wire Wire Line
	3450 1550 3600 1550
Wire Wire Line
	3600 1550 3650 1550
Wire Wire Line
	3350 1550 3350 1600
Wire Wire Line
	3350 1550 3450 1550
Wire Wire Line
	6625 8150 6775 8150
Wire Wire Line
	6625 7600 6625 7625
Wire Wire Line
	6625 6975 6775 6975
Wire Wire Line
	6625 3675 6775 3675
Wire Wire Line
	6275 8150 6300 8150
Wire Wire Line
	5225 7700 5325 7700
Wire Wire Line
	6275 6975 6300 6975
Wire Wire Line
	5650 6575 5650 6975
Wire Wire Line
	6275 5275 6300 5275
Wire Wire Line
	5650 4875 5650 5275
Wire Wire Line
	4575 4775 4600 4775
Wire Wire Line
	4875 2825 4900 2825
Wire Wire Line
	6275 3675 6300 3675
Wire Wire Line
	5650 3275 5650 3675
Wire Wire Line
	5525 2925 5650 2925
Wire Wire Line
	1675 8625 1675 8725
Wire Wire Line
	1800 8825 2000 8825
Wire Wire Line
	2000 8825 2200 8825
Wire Wire Line
	2200 8325 2200 8425
Wire Wire Line
	5225 4875 5325 4875
Wire Wire Line
	4150 4775 4150 4900
Wire Wire Line
	5225 6575 5325 6575
Wire Wire Line
	4575 6475 4600 6475
Wire Wire Line
	1650 8525 1800 8525
Wire Wire Line
	5425 1350 5525 1350
Wire Wire Line
	5525 1850 5700 1850
Wire Wire Line
	5000 1850 5525 1850
Wire Wire Line
	5700 1600 5700 1350
Wire Wire Line
	5700 1800 5700 1850
Connection ~ 5700 1850
Wire Wire Line
	5700 1850 6050 1850
Wire Wire Line
	6350 850  6350 1100
Wire Wire Line
	4900 1975 5150 1975
Wire Wire Line
	5150 1975 5150 1450
Connection ~ 5150 1450
Wire Wire Line
	5150 1450 5200 1450
Wire Wire Line
	5525 1600 5525 1450
Wire Wire Line
	5525 1450 5500 1450
Wire Wire Line
	5525 1450 5525 1350
Connection ~ 5525 1450
Connection ~ 5525 1350
Wire Wire Line
	5525 1350 5700 1350
Wire Wire Line
	3900 9800 4175 9800
Wire Wire Line
	4125 9450 4175 9450
Wire Wire Line
	4175 9550 4175 9450
Wire Wire Line
	4150 9000 4325 9000
Wire Wire Line
	4325 8900 4175 8900
Connection ~ 4175 8900
Wire Wire Line
	4175 8900 4175 9450
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C1
U 1 1 5C21A3EA
P 6525 9025
F 0 "C1" H 6535 9095 50  0000 L CNN
F 1 "100nF" H 6535 8945 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6525 9025 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 6525 9025 50  0001 C CNN
	1    6525 9025
	1    0    0    -1  
$EndComp
Wire Wire Line
	6525 8925 6525 8850
Connection ~ 6525 8850
Wire Wire Line
	6525 8850 6725 8850
Wire Wire Line
	5000 4475 5000 4450
$Comp
L Components:MCP6004 U1
U 1 1 5BD04329
P 4900 4775
F 0 "U1" H 4900 4975 50  0000 L CNN
F 1 "MCP6004" H 4875 4625 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4850 4875 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/Free-Shipping-100pcs-lots-MCP6004-I-SL-MCP6004-SOP-14-IC-In-stock/1380782_32850180423.html" H 4950 4975 50  0001 C CNN
	1    4900 4775
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 5275 5000 5075
$Comp
L 3Phase-rescue:R-3Phase-rescue R38
U 1 1 5C1EE790
P 1925 5025
F 0 "R38" V 2005 5025 50  0000 C CNN
F 1 "39h" V 1925 5025 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1855 5025 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 1925 5025 50  0001 C CNN
	1    1925 5025
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R55
U 1 1 5C1EFC16
P 1825 6425
F 0 "R55" V 1905 6425 50  0000 C CNN
F 1 "180h" V 1825 6425 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1755 6425 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/300pcs-1-0603-SMD-resistor-0R-10M-1-10W-0-0-1-1-10-100-150/1361740_32867298442.html" H 1825 6425 50  0001 C CNN
	1    1825 6425
	0    -1   1    0   
$EndComp
Wire Wire Line
	1975 6425 2225 6425
$Comp
L 3Phase-rescue:R-3Phase-rescue R65
U 1 1 5C207DDF
P 6775 3425
F 0 "R65" V 6855 3425 50  0000 C CNN
F 1 "1M" V 6775 3425 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6705 3425 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6775 3425 50  0001 C CNN
	1    6775 3425
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R66
U 1 1 5C207F21
P 6625 3425
F 0 "R66" V 6705 3425 50  0000 C CNN
F 1 "1M" V 6625 3425 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6555 3425 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6625 3425 50  0001 C CNN
	1    6625 3425
	1    0    0    1   
$EndComp
Wire Wire Line
	6625 3675 6625 3575
Connection ~ 6625 3675
Wire Wire Line
	6775 3575 6775 3675
Connection ~ 6775 3675
Wire Wire Line
	6775 3675 6975 3675
Wire Wire Line
	7000 4575 6975 4575
$Comp
L 3Phase-rescue:R-3Phase-rescue R41
U 1 1 5C1A194C
P 6625 5000
F 0 "R41" V 6550 5000 50  0000 C CNN
F 1 "1M" V 6625 5000 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6555 5000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6625 5000 50  0001 C CNN
	1    6625 5000
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R42
U 1 1 5C1A1A90
P 6775 5000
F 0 "R42" V 6700 5000 50  0000 C CNN
F 1 "1M" V 6775 5000 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6705 5000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-360K-390K-430K-470K-510K-560K/624710_32950384695.html" H 6775 5000 50  0001 C CNN
	1    6775 5000
	1    0    0    1   
$EndComp
Wire Wire Line
	6275 4775 6625 4775
Wire Wire Line
	5200 4675 6000 4675
Wire Wire Line
	6600 5275 6625 5275
Wire Wire Line
	6775 5150 6775 5275
Connection ~ 6775 5275
Wire Wire Line
	6775 5275 6875 5275
Wire Wire Line
	6625 5150 6625 5275
Connection ~ 6625 5275
Wire Wire Line
	6625 5275 6775 5275
Wire Wire Line
	5200 6375 5650 6375
Wire Wire Line
	6625 6850 6625 6975
Wire Wire Line
	6775 6850 6775 6975
Connection ~ 6775 6975
Wire Wire Line
	6775 6975 6975 6975
Wire Wire Line
	5000 7900 5000 7925
Wire Wire Line
	6975 6275 7000 6275
Wire Wire Line
	5200 7500 5800 7500
Wire Wire Line
	6775 7925 6775 8150
Connection ~ 6775 8150
Wire Wire Line
	6775 8150 6975 8150
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C26
U 1 1 5BF092ED
P 5150 7275
F 0 "C26" H 5160 7345 50  0000 L CNN
F 1 "100nF" H 5160 7195 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5150 7275 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 5150 7275 50  0001 C CNN
	1    5150 7275
	0    1    -1   0   
$EndComp
Wire Wire Line
	5050 7275 5000 7275
Connection ~ 5000 7275
Wire Wire Line
	5000 7275 5000 7200
$Comp
L power:GND #PWR0103
U 1 1 5BFBA1DC
P 5300 7275
F 0 "#PWR0103" H 5300 7025 50  0001 C CNN
F 1 "GND" V 5200 7200 50  0000 C CNN
F 2 "" H 5300 7275 50  0000 C CNN
F 3 "" H 5300 7275 50  0000 C CNN
	1    5300 7275
	0    -1   1    0   
$EndComp
Wire Wire Line
	5300 7275 5250 7275
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C25
U 1 1 5BFF8F92
P 5125 4450
F 0 "C25" H 5135 4520 50  0000 L CNN
F 1 "100nF" H 5135 4370 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5125 4450 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 5125 4450 50  0001 C CNN
	1    5125 4450
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C03434A
P 5250 4450
F 0 "#PWR0104" H 5250 4200 50  0001 C CNN
F 1 "GND" V 5150 4375 50  0000 C CNN
F 2 "" H 5250 4450 50  0000 C CNN
F 3 "" H 5250 4450 50  0000 C CNN
	1    5250 4450
	0    -1   1    0   
$EndComp
Wire Wire Line
	5250 4450 5225 4450
Wire Wire Line
	5025 4450 5000 4450
Connection ~ 5000 4450
Wire Wire Line
	5000 4450 5000 4400
$Comp
L power:GND #PWR0105
U 1 1 5C0CCC53
P 1950 7725
F 0 "#PWR0105" H 1950 7475 50  0001 C CNN
F 1 "GND" V 1950 7525 50  0000 C CNN
F 2 "" H 1950 7725 50  0000 C CNN
F 3 "" H 1950 7725 50  0000 C CNN
	1    1950 7725
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5C0CECC4
P 1950 7625
F 0 "#PWR0106" H 1950 7475 50  0001 C CNN
F 1 "+5V" V 1950 7825 50  0000 C CNN
F 2 "" H 1950 7625 50  0000 C CNN
F 3 "" H 1950 7625 50  0000 C CNN
	1    1950 7625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 7625 1950 7625
Wire Wire Line
	1950 7725 2300 7725
$Comp
L Transistor_FET:2N7002 Q2
U 1 1 5C95A6F5
P 2175 3375
F 0 "Q2" H 2380 3421 50  0000 L CNN
F 1 "2N7002" H 2380 3330 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2375 3300 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7002.pdf" H 2175 3375 50  0001 L CNN
	1    2175 3375
	1    0    0    -1  
$EndComp
Wire Notes Line
	575  2325 7900 2325
Wire Wire Line
	1800 6875 1650 6875
Wire Wire Line
	2200 6975 1650 6975
Wire Wire Line
	1800 7075 1650 7075
Wire Wire Line
	2200 7175 1650 7175
Wire Wire Line
	1800 7275 1650 7275
Wire Wire Line
	1800 7475 1650 7475
Wire Wire Line
	2200 7375 1650 7375
Wire Wire Line
	1675 8825 1675 8850
Wire Wire Line
	1650 8725 1675 8725
Connection ~ 1675 8725
Wire Wire Line
	1675 8725 1675 8825
Text GLabel 2175 6775 2    60   Input ~ 0
BAT_ADC
Wire Wire Line
	2175 6775 1650 6775
Text GLabel 1800 6675 2    60   Input ~ 0
CHRG
Text GLabel 2300 3875 2    60   Input ~ 0
EXT
Wire Wire Line
	1800 6675 1650 6675
Wire Wire Line
	1675 6425 1650 6425
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C28
U 1 1 5C17FA70
P 2150 4875
F 0 "C28" H 2160 4945 50  0000 L CNN
F 1 "100nF" H 2160 4795 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2150 4875 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 2150 4875 50  0001 C CNN
	1    2150 4875
	0    1    -1   0   
$EndComp
Wire Wire Line
	2050 4875 1675 4875
Wire Wire Line
	1675 4750 1675 4875
Wire Wire Line
	2425 4750 2475 4750
$Comp
L 3Phase-rescue:R-3Phase-rescue R44
U 1 1 5BF49798
P 2025 4000
F 0 "R44" V 2105 4000 50  0000 C CNN
F 1 "100k" H 2025 4000 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1955 4000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 2025 4000 50  0001 C CNN
	1    2025 4000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5BF49924
P 2350 4000
F 0 "#PWR0109" H 2350 3750 50  0001 C CNN
F 1 "GND" H 2325 3850 50  0000 C CNN
F 2 "" H 2350 4000 50  0000 C CNN
F 3 "" H 2350 4000 50  0000 C CNN
	1    2350 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2175 4000 2250 4000
Wire Wire Line
	1875 4000 1700 4000
Wire Wire Line
	1700 4000 1700 4175
Wire Wire Line
	1700 4175 1650 4175
Wire Wire Line
	2300 3875 1650 3875
Text GLabel 1750 3775 2    60   Input ~ 0
POWER_EN
Wire Wire Line
	1750 3775 1700 3775
$Comp
L 3Phase-rescue:R-3Phase-rescue R37
U 1 1 5C1A62AF
P 2000 3600
F 0 "R37" V 2080 3600 50  0000 C CNN
F 1 "100k" H 2000 3600 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1930 3600 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 2000 3600 50  0001 C CNN
	1    2000 3600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5C1A6403
P 2375 3600
F 0 "#PWR0110" H 2375 3350 50  0001 C CNN
F 1 "GND" H 2250 3525 50  0000 C CNN
F 2 "" H 2375 3600 50  0000 C CNN
F 3 "" H 2375 3600 50  0000 C CNN
	1    2375 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 3600 1700 3600
Wire Wire Line
	1700 3600 1700 3775
Connection ~ 1700 3775
Wire Wire Line
	1700 3775 1650 3775
Wire Wire Line
	2550 6425 2525 6425
Wire Wire Line
	1775 5025 1650 5025
Wire Wire Line
	2075 5025 2625 5025
Wire Wire Line
	2625 5025 2625 5225
Wire Wire Line
	2600 5675 2625 5675
Connection ~ 2625 5675
Wire Wire Line
	2625 5675 2625 6125
Wire Wire Line
	2600 5225 2625 5225
Connection ~ 2625 5225
Wire Wire Line
	2625 5225 2625 5675
Wire Wire Line
	2000 5225 1650 5225
Wire Wire Line
	1650 5675 2000 5675
Wire Wire Line
	2000 6125 1650 6125
Wire Wire Line
	1975 3375 1650 3375
Wire Wire Line
	2150 3600 2275 3600
Wire Wire Line
	2275 3575 2275 3600
Connection ~ 2275 3600
Wire Wire Line
	2275 3600 2375 3600
$Comp
L power:+5V #PWR0112
U 1 1 5CA1F349
P 2050 3200
F 0 "#PWR0112" H 2050 3050 50  0001 C CNN
F 1 "+5V" V 2050 3400 50  0000 C CNN
F 2 "" H 2050 3200 50  0000 C CNN
F 3 "" H 2050 3200 50  0000 C CNN
	1    2050 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2050 3200 2075 3200
Wire Wire Line
	2075 3200 2075 3150
Wire Wire Line
	1800 8525 2325 8525
Wire Wire Line
	2000 8575 2000 8425
Wire Wire Line
	4925 9100 5200 9100
Wire Wire Line
	5425 8600 5550 8600
Connection ~ 5550 8600
Wire Wire Line
	5550 8600 7325 8600
Wire Wire Line
	5875 9750 5875 9800
Wire Wire Line
	5875 9800 6200 9800
Wire Wire Line
	6725 8900 6725 8850
Connection ~ 5875 9800
Connection ~ 6525 9800
Wire Wire Line
	6525 9800 6600 9800
$Comp
L power:+3.3V #PWR0113
U 1 1 5DD49C09
P 6225 9450
F 0 "#PWR0113" H 6225 9300 50  0001 C CNN
F 1 "+3.3V" V 6300 9475 50  0000 C CNN
F 2 "" H 6225 9450 50  0000 C CNN
F 3 "" H 6225 9450 50  0000 C CNN
	1    6225 9450
	0    1    1    0   
$EndComp
Wire Wire Line
	6225 9450 6200 9450
$Comp
L 3Phase-rescue:D_Small_ALT-3Phase-rescue D13
U 1 1 5DFFCEE7
P 5550 8725
F 0 "D13" H 5500 8805 50  0000 L CNN
F 1 "1N5819WS" H 5400 8645 50  0001 L CNN
F 2 "Diode_SMD:D_SOD-323" V 5550 8725 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-Diode-1N4001-M1-1N4005-M5-1N4007-M7-1N4004-M4-SR160-1N4148-1N4148-1N5819-RS1M-RS2M/730231_32868492349.html" V 5550 8725 50  0001 C CNN
	1    5550 8725
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0114
U 1 1 5E131EA4
P 2325 8425
F 0 "#PWR0114" H 2325 8275 50  0001 C CNN
F 1 "+3.3V" V 2325 8675 50  0000 C CNN
F 2 "" H 2325 8425 50  0000 C CNN
F 3 "" H 2325 8425 50  0000 C CNN
	1    2325 8425
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 8425 2200 8425
Connection ~ 2000 8425
Connection ~ 2200 8425
Wire Wire Line
	2200 8425 2200 8575
Wire Wire Line
	2200 8425 2325 8425
Wire Wire Line
	2275 3150 2275 3175
$Comp
L Components:Conn_01x03 J5
U 1 1 5E457107
P 2500 2775
F 0 "J5" H 2580 2817 50  0000 L CNN
F 1 "Conn_01x03" H 2580 2726 50  0000 L CNN
F 2 "Footprints:PinHeader_1x03_P2.54mm_Vertical" H 2500 2775 50  0001 C CNN
F 3 "~" H 2500 2775 50  0001 C CNN
	1    2500 2775
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E55C7EA
P 2000 2875
F 0 "#PWR0108" H 2000 2625 50  0001 C CNN
F 1 "GND" V 2000 2700 50  0000 C CNN
F 2 "" H 2000 2875 50  0000 C CNN
F 3 "" H 2000 2875 50  0000 C CNN
	1    2000 2875
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 2875 2000 2875
$Comp
L 3Phase-rescue:R-3Phase-rescue R39
U 1 1 5E8C6875
P 1850 4750
F 0 "R39" V 1930 4750 50  0000 C CNN
F 1 "10k" H 1850 4750 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1780 4750 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1-6-0-8mm-0603-1-1-10W-1608-51K-56K-62K-68K-75K-82K/624710_32953491687.html" H 1850 4750 50  0001 C CNN
	1    1850 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 4750 1675 4750
Wire Wire Line
	2000 4750 2025 4750
Wire Wire Line
	2100 8225 2200 8225
Wire Wire Line
	2200 8225 2200 8325
$Comp
L 3Phase-rescue:XC6206-3Phase-rescue U4
U 1 1 5D1E085F
P 5875 9550
F 0 "U4" H 5975 9350 50  0000 C CNN
F 1 "XC6206" H 5875 9850 50  0001 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5875 9650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-XC6206P332MR-SOT23-3-XC6206P332-SOT23-XC6206-SMD-662K-3-3V-0-5A-Positive-Fixed-LDO/1969251_32947788534.html" H 5875 9650 50  0001 C CNN
	1    5875 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 9800 5050 9800
$Comp
L 3Phase-rescue:D_Small_ALT-3Phase-rescue D14
U 1 1 5C0D8D38
P 4475 9450
F 0 "D14" H 4425 9530 50  0000 L CNN
F 1 "1N5819WS" H 4325 9370 50  0001 L CNN
F 2 "Diode_SMD:D_SOD-323" V 4475 9450 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-Diode-1N4001-M1-1N4005-M5-1N4007-M7-1N4004-M4-SR160-1N4148-1N4148-1N5819-RS1M-RS2M/730231_32868492349.html" V 4475 9450 50  0001 C CNN
	1    4475 9450
	-1   0    0    1   
$EndComp
Wire Wire Line
	4175 9450 4375 9450
Wire Wire Line
	4575 9450 5050 9450
Wire Wire Line
	5550 8825 5550 8850
Connection ~ 5550 9450
Wire Wire Line
	5550 9450 5575 9450
Connection ~ 2200 8325
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C29
U 1 1 5C6260A8
P 5050 9625
F 0 "C29" H 5060 9695 50  0000 L CNN
F 1 "100nF" H 5060 9545 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5050 9625 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 5050 9625 50  0001 C CNN
	1    5050 9625
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 9525 5050 9450
Connection ~ 5050 9450
Wire Wire Line
	5050 9450 5550 9450
Wire Wire Line
	5050 9725 5050 9800
Connection ~ 5050 9800
Wire Wire Line
	5050 9800 5200 9800
$Comp
L power:+3.3V #PWR0115
U 1 1 5C6C4D32
P 2475 4750
F 0 "#PWR0115" H 2475 4600 50  0001 C CNN
F 1 "+3.3V" V 2475 4975 50  0000 C CNN
F 2 "" H 2475 4750 50  0000 C CNN
F 3 "" H 2475 4750 50  0000 C CNN
	1    2475 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 8600 5550 8625
Wire Wire Line
	6525 9125 6525 9800
Connection ~ 6200 9450
Wire Wire Line
	6200 9450 6175 9450
Wire Wire Line
	2250 4875 2475 4875
Wire Wire Line
	1650 4875 1675 4875
Connection ~ 1675 4875
$Comp
L power:+5V #PWR0101
U 1 1 5D194434
P 5000 7200
F 0 "#PWR0101" H 5000 7050 50  0001 C CNN
F 1 "+5V" V 5075 7250 50  0000 C CNN
F 2 "" H 5000 7200 50  0000 C CNN
F 3 "" H 5000 7200 50  0000 C CNN
	1    5000 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6725 8850 7300 8850
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C30
U 1 1 5D19FBA6
P 2300 9125
F 0 "C30" H 2310 9195 50  0000 L CNN
F 1 "100nF" H 2310 9045 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2300 9125 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 2300 9125 50  0001 C CNN
	1    2300 9125
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5D19FF54
P 2475 9125
F 0 "#PWR0117" H 2475 8875 50  0001 C CNN
F 1 "GND" H 2475 8975 50  0000 C CNN
F 2 "" H 2475 9125 50  0000 C CNN
F 3 "" H 2475 9125 50  0000 C CNN
	1    2475 9125
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0118
U 1 1 5D1A14C3
P 2125 9100
F 0 "#PWR0118" H 2125 8950 50  0001 C CNN
F 1 "+5V" V 2200 9150 50  0000 C CNN
F 2 "" H 2125 9100 50  0000 C CNN
F 3 "" H 2125 9100 50  0000 C CNN
	1    2125 9100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2125 9100 2125 9125
Wire Wire Line
	2200 9125 2125 9125
Connection ~ 2125 9125
Wire Wire Line
	2125 9125 2125 9150
Wire Wire Line
	2400 9125 2475 9125
$Comp
L power:+5V #PWR0119
U 1 1 5D28B512
P 5000 4400
F 0 "#PWR0119" H 5000 4250 50  0001 C CNN
F 1 "+5V" V 5075 4450 50  0000 C CNN
F 2 "" H 5000 4400 50  0000 C CNN
F 3 "" H 5000 4400 50  0000 C CNN
	1    5000 4400
	1    0    0    -1  
$EndComp
Connection ~ 4900 1350
Wire Wire Line
	4900 1350 5125 1350
Wire Wire Line
	4900 1450 5000 1450
Connection ~ 5000 1450
Wire Wire Line
	5000 1450 5150 1450
$Comp
L Device:R_Small R40
U 1 1 5D62E090
P 4275 1725
F 0 "R40" H 4375 1750 50  0000 C CNN
F 1 "4k7" H 4275 1635 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4275 1725 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100-PCS-LOT-BZT52C15S-BZT52C15-SOD-323-123-WJ/4580010_32950079051.html" V 4275 1725 50  0001 C CNN
	1    4275 1725
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 1825 4275 1850
Connection ~ 4275 1850
Wire Wire Line
	4275 1850 4600 1850
Wire Wire Line
	4275 1625 4275 1550
Connection ~ 4275 1550
Wire Wire Line
	4275 1550 4300 1550
Wire Wire Line
	3450 1150 4000 1150
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C27
U 1 1 5D4A1360
P 7525 9525
F 0 "C27" H 7535 9595 50  0000 L CNN
F 1 "100nF" H 7535 9445 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7525 9525 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 7525 9525 50  0001 C CNN
	1    7525 9525
	1    0    0    -1  
$EndComp
Wire Wire Line
	7525 9425 7525 9350
Connection ~ 7525 9350
Wire Wire Line
	7525 9350 7600 9350
Wire Wire Line
	7525 9625 7525 9800
Wire Wire Line
	2900 9700 2900 9675
Wire Wire Line
	6725 9800 7525 9800
Wire Wire Line
	1650 8225 1800 8225
$Comp
L 3Phase-rescue:CP_Small-3Phase-rescue C31
U 1 1 5DA3AF22
P 6200 9650
F 0 "C31" H 6210 9720 50  0000 L CNN
F 1 "100uF 16v" H 6210 9570 50  0001 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6200 9650 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-lot-Electrolytic-Capacitors-10V-100uf-10V-Electrolytic-Capacitor-size-4mm-7mm/1021664_1481491846.html" H 6200 9650 50  0001 C CNN
	1    6200 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 9800 5875 9800
Wire Wire Line
	6200 9750 6200 9800
Connection ~ 6200 9800
Wire Wire Line
	6200 9800 6400 9800
Wire Wire Line
	6200 9550 6200 9450
$Comp
L power:GND #PWR0102
U 1 1 5DB9F2FC
P 2125 9775
F 0 "#PWR0102" H 2125 9525 50  0001 C CNN
F 1 "GND" H 2225 9775 50  0000 C CNN
F 2 "" H 2125 9775 50  0000 C CNN
F 3 "" H 2125 9775 50  0000 C CNN
	1    2125 9775
	1    0    0    -1  
$EndComp
Wire Wire Line
	2125 9775 2125 9750
$Comp
L power:GND #PWR0111
U 1 1 5C28DFF1
P 2550 6425
F 0 "#PWR0111" H 2550 6175 50  0001 C CNN
F 1 "GND" H 2425 6350 50  0000 C CNN
F 2 "" H 2550 6425 50  0000 C CNN
F 3 "" H 2550 6425 50  0000 C CNN
	1    2550 6425
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_FET:2N7002 Q3
U 1 1 5D9F658F
P 2150 4225
F 0 "Q3" H 2355 4271 50  0000 L CNN
F 1 "2N7002" H 2355 4180 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2350 4150 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7002.pdf" H 2150 4225 50  0001 L CNN
	1    2150 4225
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 4025 2250 4000
Connection ~ 2250 4000
Wire Wire Line
	2250 4000 2350 4000
Wire Wire Line
	1950 4225 1700 4225
Wire Wire Line
	1700 4225 1700 4175
Connection ~ 1700 4175
$Comp
L Components:Conn_01x02 J6
U 1 1 5DB54C15
P 2575 4425
F 0 "J6" H 2655 4467 50  0000 L CNN
F 1 "Conn_01x03" H 2655 4376 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 2575 4425 50  0001 C CNN
F 3 "~" H 2575 4425 50  0001 C CNN
	1    2575 4425
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 4425 2375 4425
$Comp
L power:+5V #PWR0107
U 1 1 5DC70A3A
P 2350 4525
F 0 "#PWR0107" H 2350 4375 50  0001 C CNN
F 1 "+5V" V 2350 4725 50  0000 C CNN
F 2 "" H 2350 4525 50  0000 C CNN
F 3 "" H 2350 4525 50  0000 C CNN
	1    2350 4525
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2375 4525 2350 4525
$Comp
L 3Phase-rescue:XC6206-3Phase-rescue U2
U 1 1 5DDDB6A8
P 5875 8950
F 0 "U2" H 5975 8750 50  0000 C CNN
F 1 "XC6206" H 5875 9250 50  0001 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5875 9050 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-XC6206P332MR-SOT23-3-XC6206P332-SOT23-XC6206-SMD-662K-3-3V-0-5A-Positive-Fixed-LDO/1969251_32947788534.html" H 5875 9050 50  0001 C CNN
	1    5875 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5575 8850 5550 8850
Connection ~ 5550 8850
Wire Wire Line
	5550 8850 5550 9450
Wire Wire Line
	6175 8850 6400 8850
$Comp
L power:GND #PWR0116
U 1 1 5E58F5CC
P 5875 9175
F 0 "#PWR0116" H 5875 8925 50  0001 C CNN
F 1 "GND" H 5750 9075 50  0000 C CNN
F 2 "" H 5875 9175 50  0000 C CNN
F 3 "" H 5875 9175 50  0000 C CNN
	1    5875 9175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5875 9150 5875 9175
$Comp
L 3Phase-rescue:CP_Small-3Phase-rescue C32
U 1 1 5E5D681A
P 6400 9225
F 0 "C32" H 6410 9295 50  0000 L CNN
F 1 "100uF 16v" H 6410 9145 50  0001 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6400 9225 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-lot-Electrolytic-Capacitors-10V-100uf-10V-Electrolytic-Capacitor-size-4mm-7mm/1021664_1481491846.html" H 6400 9225 50  0001 C CNN
	1    6400 9225
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 9125 6400 8850
Connection ~ 6400 8850
Wire Wire Line
	6400 8850 6525 8850
Wire Wire Line
	6400 9325 6400 9800
Connection ~ 6400 9800
Wire Wire Line
	6400 9800 6525 9800
Wire Wire Line
	6775 7425 5800 7425
Wire Wire Line
	5800 7425 5800 7500
Wire Wire Line
	5650 7700 5650 8150
Wire Wire Line
	6775 7425 6775 7600
Wire Wire Line
	7000 7600 6775 7600
Connection ~ 6775 7600
Wire Wire Line
	6775 7600 6775 7625
Wire Wire Line
	6625 7600 6625 7500
Wire Wire Line
	6625 7500 7000 7500
Connection ~ 6625 7600
Wire Wire Line
	4525 7600 4575 7600
Wire Wire Line
	4825 8150 4575 8150
Wire Wire Line
	4575 8150 4575 7600
Connection ~ 4575 7600
Wire Wire Line
	4575 7600 4600 7600
Wire Wire Line
	6775 6300 6775 6475
Wire Wire Line
	5650 6300 6775 6300
Wire Wire Line
	7000 6475 6775 6475
Connection ~ 6775 6475
Wire Wire Line
	6775 6475 6775 6550
Wire Wire Line
	7000 6375 6625 6375
Wire Wire Line
	6625 6375 6625 6475
Wire Wire Line
	6275 6475 6625 6475
Connection ~ 6625 6475
Wire Wire Line
	6625 6475 6625 6550
Wire Wire Line
	4800 6975 4575 6975
Wire Wire Line
	4575 6475 4575 6975
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C33
U 1 1 5F640827
P 6975 8000
F 0 "C33" H 6985 8070 50  0000 L CNN
F 1 "100nF" H 6985 7920 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6975 8000 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 6975 8000 50  0001 C CNN
	1    6975 8000
	1    0    0    1   
$EndComp
Wire Wire Line
	6975 7900 6975 7700
Connection ~ 6975 7700
Wire Wire Line
	6975 7700 7000 7700
Wire Wire Line
	6975 8100 6975 8150
Connection ~ 6975 8150
Wire Wire Line
	6975 8150 7175 8150
Wire Wire Line
	6775 4600 6000 4600
Wire Wire Line
	6000 4600 6000 4675
Wire Wire Line
	7000 4775 6775 4775
Wire Wire Line
	6775 4775 6775 4850
Wire Wire Line
	6775 4600 6775 4775
Connection ~ 6775 4775
Wire Wire Line
	7000 4675 6625 4675
Wire Wire Line
	6625 4675 6625 4775
Connection ~ 6625 4775
Wire Wire Line
	4575 5225 5225 5225
Wire Wire Line
	4875 3275 5525 3275
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C34
U 1 1 5FB7F377
P 6975 3525
F 0 "C34" H 6985 3595 50  0000 L CNN
F 1 "100nF" H 6985 3445 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6975 3525 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-1nF-10nF-100nF-0-1uF-1uF-10uF-0603-X7R-Error-10-SMD-Thick-Film-Chip/1962508_32923124783.html" H 6975 3525 50  0001 C CNN
	1    6975 3525
	1    0    0    1   
$EndComp
Wire Wire Line
	6975 3625 6975 3675
Connection ~ 6975 3675
Wire Wire Line
	6975 3675 7125 3675
Wire Wire Line
	6975 3425 6975 3275
Connection ~ 6975 3275
Wire Wire Line
	6975 3275 7000 3275
Wire Wire Line
	7000 3175 6775 3175
Wire Wire Line
	6775 3175 6775 2725
Wire Wire Line
	5500 2725 6775 2725
Connection ~ 6775 3175
Wire Wire Line
	7000 3075 6625 3075
Wire Wire Line
	6625 3075 6625 3175
Wire Wire Line
	6275 3175 6625 3175
Connection ~ 6625 3175
Wire Wire Line
	6625 3175 6625 3275
Wire Wire Line
	1650 2775 2100 2775
Wire Wire Line
	2100 2775 2100 2675
Wire Wire Line
	2100 2675 2300 2675
Wire Wire Line
	2300 2775 2175 2775
Wire Wire Line
	2175 2775 2175 2725
Wire Wire Line
	2175 2725 2025 2725
Wire Wire Line
	2025 2725 2025 2675
Wire Wire Line
	2025 2675 1650 2675
$EndSCHEMATC
