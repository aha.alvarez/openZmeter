Description
===============

openzmeter (oZm) is an Open Source system designed for power quality analysis and smart energy metering in power networks.
It has been designed to comply with international standards IEC 61000-4-30 and EN 50160. It is able to measure RMS voltages with an accuracy up to 0.1%, frequency up to 10 mHz (between 42.5 and 57.5 Hz) and RMS currents according to different sensor probes (the basic version can handle up to 400 V and 50 A RMS using an onboard Hall effect sensor with a precision of 1%).

See http://www.openzmeter.com